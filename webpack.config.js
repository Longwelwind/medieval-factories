const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = (env, argv) => {
	let mode = argv !== undefined && argv.mode !== undefined ? argv.mode : "development";

	// Extracting env variables
	let GA_API_GAME = env !== undefined ? env.GA_API_GAME : undefined;
	let GA_API_KEY = env !== undefined ? env.GA_API_KEY : undefined;

	return {
		entry: {
			index: "./scripts/script.tsx",
			edit: "./scripts/script-edit.tsx"
		},
		output: {
			path: __dirname,
			filename: "bundle-[name].js"
		},
		resolve: {
			extensions: [".ts", ".tsx", ".js"]
		},
		module: {
			rules: [
				{
					test: /\.tsx?$/,
					loader: "ts-loader",
					exclude: /node_modules/
				},
				{
					enforce: 'pre',
					test: /\.js$/,
					loader: "source-map-loader"
				},
				{
					enforce: 'pre',
					test: /\.tsx?$/,
					use: "source-map-loader"
				}
			]
		},
		optimization: mode === "production" ? {
			minimizer: [
				new UglifyJsPlugin({
					uglifyOptions: {
						mangle: {
							keep_classnames: true,
							keep_fnames: true
						}
					}
				})
			]
		} : {

		},
		devtool: mode === "development" ? "inline-source-map" : undefined,
		mode: mode,
        plugins: [
        	new webpack.DefinePlugin({
				env: {
					GA_API_GAME: JSON.stringify(GA_API_GAME),
					GA_API_KEY: JSON.stringify(GA_API_KEY),
					DEBUG: mode === "development"
				}
			})
		]
    }
};