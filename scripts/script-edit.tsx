import * as Util from "./game/Util";
import GameData from "./game/GameData";
import Item from "./game/Item";
import ItemQuantity from "./game/ItemQuantity";
import Pack from "./game/Pack";
import Recipe from "./game/Recipe";
import EditApp from "./ui/EditApp";
import { Serializer } from "./serialization/Serialization";

import * as React from "react";
import * as ReactDOM from "react-dom";
import PackTree from "./game/PackTree";
import createGameSerializer from "./createGameSerializer";

function start(gameData) {
	ReactDOM.render(<EditApp gameData={gameData} />, document.getElementById("root"));

	window["gameData"] = gameData;
}

let serializer = createGameSerializer();

// Try to load locally
let gameDataRaw = window.localStorage.getItem("savedGameData");
if (gameDataRaw == null) {
	console.log("Fetching official data.json");
	Util.httpGetAsync("data.json", (status, response) => {
		let gameData;
		if (status == 200 && response != "") {
			console.log("data.json found!");
			var gameDataRaw = JSON.parse(response);
			gameData = serializer.deserialize<GameData>(gameDataRaw, false);
		} else {
			console.log("data.json not found or empty, creating from blank");
			gameData = new GameData();
		}

		start(gameData);
	});
}
console.log("Loading saved game data");
let gameDataJson = JSON.parse(gameDataRaw);
let gameData = serializer.deserialize<GameData>(gameDataJson, false);
start(gameData);