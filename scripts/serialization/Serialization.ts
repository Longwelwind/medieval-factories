import IDable from "./IDable";

export class Serializer {
	private readonly NON_SERIALIZED_PROPERTY_NAME = "nonSerializedProperties";

	classes: any[];
	classListManager: ClassListManager;

	// classes is "any[]" because it is not possible at the moment
	// to find a type that matches the various constructor
	public constructor(classes: any[]) {
		this.classListManager = new ClassListManager();
		this.classes = classes;
	}

	public serialize(root: IDable): SerializedData {
		// We only want to include the instances that were "touched"
		// during this pass of serialization. Thus, we mark each
		// of them with a marker to retrieve them later
		let rootReference = this.serializeObject(root);

		let filteredClassLists: ClassList[] = [];
		this.classListManager.classLists.forEach((cl) => {
			let toIncludeInstances = cl.instances.filter((i) => i.touchedThisPass && !i.neverSerialize);
			if (toIncludeInstances.length > 0) {
				filteredClassLists.push({
					className: cl.className,
					nextId: cl.nextId,
					instances: toIncludeInstances
				});
			}

			cl.instances.forEach((i) => i.touchedThisPass = false);
		});

		// Since the return value is destined to be stringified into JSON
		// for example, we filter out some unecessary value
		filteredClassLists = filteredClassLists.map((cl) => {
			return {
				className: cl.className,
				nextId: cl.nextId,
				instances: cl.instances.map((ce) => {
					return {
						id: ce.id,
						dataObject: ce.dataObject
					}
				})
			}
		});

		return {
			rootReference: rootReference,
			classLists: filteredClassLists
		};
	}

	private serializeObject(instance: IDable): Reference {
		let className = instance.constructor.name;

		// We first check if this instance isn't already being serialized
		// (in case of cycle reference, for example)
		let {id, alreadyTouched} = this.saveInstance(className, instance);

		if (!alreadyTouched) {
			// The class may have an attribute "nonSerializedProperties"
			// containing a list of attributes to ignore when serializing
			let ignoredProperties = [];
			if (instance.hasOwnProperty(this.NON_SERIALIZED_PROPERTY_NAME)) {
				ignoredProperties = instance[this.NON_SERIALIZED_PROPERTY_NAME];
			}

			// Make copy of this instance's properties into an object
			let dataObject = {};
			for (let property in instance) {
				if (
					property == this.NON_SERIALIZED_PROPERTY_NAME
					|| ignoredProperties.indexOf(property) != -1
				) {
					continue;
				}

				let value = instance[property];

				let serializedValue = this.serializeValue(value);
				dataObject[property] = serializedValue;
			}

			this.setDataObject(className, instance, dataObject);
		}

		return {
			className: instance.constructor.name,
			id: id
		};
	}

	private serializeValue(value: any): any {
		switch (typeof value) {
			case "object":
				if (value != null) {
					if (value instanceof Array) {
						return this.serializeArray(value);
					} else {
						let reference = this.serializeObject(value);

						// Return an object that represents a reference to an other serialized object
						return reference;
					}
				} else {
					return null;
				}
			case "boolean":
			case "number":
			case "string":
				return value;
			case "symbol":
			case "function":
			case "undefined":
				// Nothing, for the moment...
				return undefined;
		}
	}

	private serializeArray(arr: Array<any>) {
		return arr.map((value) => this.serializeValue(value));
	}

	private saveInstance(className: string, instance: IDable): {alreadyTouched: boolean, id: number} {
		let cl = this.classListManager.getClassList(className);

		// We first check if this class wasn't already saved before
		let ce = cl.instances.find((ce) => ce.instance == instance);
		if (ce != null) {
			let alreadyTouched = ce.touchedThisPass;
			ce.touchedThisPass = true;
			return {
				alreadyTouched: alreadyTouched,
				id: ce.id,
			}
		}
		
		// Otherwise, save it in our instances list
		// Find an ID for this object. 2 possibilities:
		let id;
		if (instance.id != null) {
			// The instance has a non-null value for the property `id`
			id = instance.id;
		} else {
			// If not, we generate a numbered incremental id
			id = cl.nextId;
			cl.nextId++;
		}
		cl.instances.push({
			id: id,
			instance: instance,
			dataObject: null,
			touchedThisPass: true,
			neverSerialize: false
		});

		return {
			alreadyTouched: false,
			id: id
		}
	}

	private setDataObject(className: string, instance: Object, dataObject: any) {
		let cl = this.classListManager.getClassList(className);
		let ce = cl.instances.find((ce) => ce.instance == instance);

		ce.dataObject = dataObject;
	}

	/**
	 * neverSerialize indicates that the objects deserialized by
	 * this method will not be reseralized again in a future serialize.
	 * It is usefull for GameData, which souldn't be bundled into the JSON
	 * produced by a future save (as those data are loaded through data.json,
	 * and not through localStorage)
	 */
	public deserialize<E>(data: SerializedData, neverSerialize: boolean): E {
		let rootReference = data.rootReference;
		
		// We merge the class lists we have been given in this 
		// payload
		this.classListManager.merge(data.classLists, neverSerialize);

		// We make a first pass to instantiate all the instances
		this.classListManager.classLists.forEach((cl) => {
			cl.instances.forEach((classEntry) => {
				if (classEntry.instance == null) {
					classEntry.instance = this.deserializeObject(cl.className, classEntry);
				}
			});
		});

		// We make a second pass to set the reference between the objects
		this.classListManager.classLists.forEach((cl) => {
			cl.instances.forEach((classEntry) => {
				// We check for ignored properties, even though
				// they shouldn't be serialized in the first place
				let ignoredProperties = [];
				if (classEntry.instance.hasOwnProperty(this.NON_SERIALIZED_PROPERTY_NAME)) {
					ignoredProperties = classEntry.instance[this.NON_SERIALIZED_PROPERTY_NAME];
				}

				for (let property in classEntry.dataObject) {
					if (
						property == this.NON_SERIALIZED_PROPERTY_NAME
						|| ignoredProperties.indexOf(property) != -1
					) {
						continue;
					}

					let value = this.deserializeValue(classEntry.dataObject[property]);
					classEntry.instance[property] = value;
				}

			});
		});

		return <E> this.resolveReference(rootReference);
	}

	private deserializeObject(className: string, dataObject: ClassEntry): IDable {
		let classConstructor = this.classes.find((c) => c.name == className);

		if (classConstructor == null) {
			throw new Error("Unknown class \"" + className + "\" found. Only available are: " + this.classes.map(c => c.name).join(", "));
		}

		// We create an empty instance of our class
		let instance: IDable = new classConstructor();
		// The properties of this instance will be set later
		// during the second pass of our deserializer
		return instance;
	}

	private resolveReference(reference: Reference): Object {
		let cl = this.classListManager.getClassList(reference.className);
		let classEntry = cl.instances.find((classEntry) => classEntry.id == reference.id);

		if (classEntry == null) {
			throw new Error("Object \"" + cl.className + "\" with id \"" + reference.id + "\" not found");
		}

		return classEntry.instance;
	}

	private deserializeArray(arr: any[]): any[] {
		return arr.map((value) => this.deserializeValue(value));
	}

	private deserializeValue(value: any): any {
		switch (typeof value) {
			case "object":
				if (value != null) {
					if (value instanceof Array) {
						return this.deserializeArray(value);
					} else {
						// This value is a reference to an other object,
						// we must find it in our database
						return this.resolveReference(value);
					}
				} else {
					return null;
				}
			case "boolean":
			case "number":
			case "string":
				return value;
			case "symbol":
			case "function":
			case "undefined":
				// Nothing, for the moment...
				return undefined;
		}
	}
}

interface SerializedData {
	rootReference: Reference,
	classLists: ClassList[]
}

interface Reference {
	className: string;
	id: number;
}

class ClassListManager {
	classLists: ClassList[] = [];

	public getClassList(className: string): ClassList {
		let cl = this.classLists.find((cl) => cl.className == className);

		if (cl == null) {
			cl = {
				className: className,
				nextId: 1,
				instances: []
			};
			this.classLists.push(cl);
		}

		return cl;
	}

	public merge(otherClassLists: ClassList[], neverSerialize: boolean) {
		otherClassLists.forEach((ocl) => {
			let cl = this.getClassList(ocl.className);
			if (cl != null) {
				// We merge the instances inside the class list
				ocl.instances.forEach((otherInstance) => {
					let instance = cl.instances.find((i) => i.id == otherInstance.id);
					if (instance == null) {
						// This is a new, not previously known instance
						otherInstance.instance = null;
						otherInstance.touchedThisPass = false;
						otherInstance.neverSerialize = neverSerialize;
						cl.instances.push(otherInstance);
					}
				});
				cl.nextId = Math.max(cl.nextId, ocl.nextId);
			} else {
				this.classLists.push(ocl);
			}
		});
	}
}

interface ClassList {
	className: string;
	nextId: number;
	instances: ClassEntry[];
}

interface ClassEntry {
	id: number;
	instance?: IDable;
	dataObject: Object;
	touchedThisPass?: boolean;
	neverSerialize?: boolean;
}