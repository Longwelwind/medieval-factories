import Item from "./game/Item";
import Recipe from "./game/Recipe";
import Pack from "./game/Pack";
import ItemQuantity from "./game/ItemQuantity";
import * as Util from "./game/Util";
import Game from "./game/Game";
import GameData from "./game/GameData";
import Worker from "./game/Worker";
import Inventory from "./game/Inventory";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { extendObservable, observable } from "mobx";
import GameApp from "./ui/GameApp";
import { Serializer } from "./serialization/Serialization";
import Analytics from "./analytics/Analytics";
import PackTree from "./game/PackTree";
import createGameSerializer from "./createGameSerializer";
// @ts-ignore TS doesn't seem to support loading JSON in Webpack
import * as gameDataRaw from "../data.json";

let rootDOMElement = document.getElementById("root");
let gameData: GameData;
let game: Game;
let analytics: Analytics;
let serializer: Serializer;

function launchGameScreen() {
	game.start();

	analytics.start(game);

	// Initializing game interface
	ReactDOM.render(<GameApp game={game} />, rootDOMElement);

	// We launch the update thread
	// Launching "thread game"
	let lastUpdated = 0;
	function update(time: number) {
		time /= 1000;
		window.requestAnimationFrame(update);
		if (lastUpdated == 0) {
			// First time
			lastUpdated = time;
			return;
		}
		let delta = time - lastUpdated;
		lastUpdated = time;

		game.update(delta);
	}
	window.requestAnimationFrame(update);

	if (env.DEBUG) {
		window["game"] = game;
	}
}

// Initializing analytics
analytics = new Analytics();
analytics.init();

// Loading the "base" data of the game (items, packs, recipes, ...)
serializer = createGameSerializer();
try {
	gameData = serializer.deserialize<GameData>(gameDataRaw, true);
} catch (e) {
	// TODO: Report on GA
	console.error(e);
}

// Dirty hack:
// We must make sure that analytics is properly loaded, but GA
// unfortunately doesn't offer any callback mecanism.
// Thus, we make a small loop that filthily checks if GA
// is initialize
let taskId = setInterval(() => {
	if (!analytics.isReady()) {
		return;
	}

	clearInterval(taskId);

	// Check whether a game is saved in localStorage
	let gameRawJson = localStorage.getItem("game");
	if (gameRawJson == undefined) {
		// No saved game
		// Create a fresh game
		game = new Game(gameData, serializer);
		game.firstStart();
	} else {
		// Saved game detected, try to parse it
		try {
			var gameRaw = JSON.parse(gameRawJson);
			game = serializer.deserialize<Game>(gameRaw, false);
		} catch (e) {
			console.error(e);
			analytics.reportError(e);
			return;
		}

		if (game.version != GameData.VERSION) {
			console.warn("Loaded game was created in version \"" + game.version + "\"");
			console.warn("But current version of game is \"" + GameData.VERSION + "\"");
			console.warn("Maybe expect errors!");
		}

	}

	launchGameScreen();
}, 100);
