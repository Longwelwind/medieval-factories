import Item from "./game/Item";
import Recipe from "./game/Recipe";
import Pack from "./game/Pack";
import ItemQuantity from "./game/ItemQuantity";
import * as Util from "./game/Util";
import Game from "./game/Game";
import GameData from "./game/GameData";
import Worker from "./game/Worker";
import Inventory from "./game/Inventory";
import {Serializer} from "./serialization/Serialization";
import PackTree from "./game/PackTree";
import Achievement from "./game/achievement/Achievement";
import {MoneyReachedCondition, PackBoughtCondition} from "./game/achievement/Condition";

export default (): Serializer => {
	return new Serializer(
		[Object, GameData, Item, Recipe, Pack, ItemQuantity,
		Game, Worker, ItemQuantity, Inventory, PackTree, Achievement,
		PackBoughtCondition, MoneyReachedCondition]
	);
}