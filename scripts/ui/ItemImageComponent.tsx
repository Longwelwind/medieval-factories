import {Component, HTMLAttributes, ReactNode} from "react";
import * as React from "react";
import Item from "../game/Item";

interface ItemImageComponentProps {
	x: number;
	y: number;
	opaque?: boolean;
}

export default class ItemImageComponent extends Component<ItemImageComponentProps & HTMLAttributes<HTMLDivElement>> {
	static defaultProps: Partial<ItemImageComponentProps & HTMLAttributes<HTMLDivElement>> = {
		opaque: true
	};

	render(): ReactNode {
		let {x, y, opaque, children, ...rest} = this.props;
		return (
			<div
				className={"item"}
				style={{
					backgroundPosition: this.getItemBackgroundPosition(x, y),
					opacity: opaque ? 1 : 0.2
				}}
				{...rest}
			>
				{children}
			</div>
		);
	}

	private getItemBackgroundPosition(x: number, y: number) {
		return (-x * 32) + "px " + (-y * 32) + "px";
	}
}