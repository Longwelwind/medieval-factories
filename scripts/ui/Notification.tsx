import Item from "../game/Item";
import Pack from "../game/Pack";
import Game from "../game/Game";
import * as Util from "../game/Util";

import * as React from "react";
import Achievement from "../game/achievement/Achievement";

export abstract class Notification {
	public constructor(public game: Game) {

	}

	abstract getItemIcon(): {x: number, y: number};
	abstract getText(): JSX.Element;
}

export class NotEnoughMoneyNotification extends Notification {
	readonly GOLD_ITEM_ID = "gold-ingot";

	public getItemIcon() {
		return this.game.gameData.items.find(i => i.id == this.GOLD_ITEM_ID);
	}

	public getText() {
		return (
			<span>You don't have enough money</span>
		);
	}
}

export class PackUnlockedNotification extends Notification {
	public constructor(game: Game, public pack: Pack) {
		super(game);
	}

	public getItemIcon(): {x: number, y: number} {
		if (this.pack.recipes.length != 0) {
			return {
				x: this.pack.recipes[0].result.x,
				y: this.pack.recipes[0].result.y
			};
		} else {
			return null;
		}
	}

	public getText() {
		return (
			<span>Pack <b>{this.pack.name}</b> unlocked !</span>
		);
	}
}

export class AchievementNotification extends Notification {
	constructor(game: Game, public achievement: Achievement) {
		super(game);
	}

	getItemIcon(): { x: number; y: number } {
		return {x: this.achievement.iconX, y: this.achievement.iconY};
	}

	getText(): JSX.Element {
		return (
			<span>
				Achievement unlocked:<br />
				<b>{this.achievement.name}</b>
			</span>
		);
	}
}
