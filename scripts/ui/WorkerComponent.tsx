import Worker from "../game/Worker";
import ItemQuantity from "../game/ItemQuantity";
import Game from "../game/Game";
import * as Util from "../game/Util";

import { observer } from "mobx-react";
import * as React from "react";
import * as cs from "react-classset";

import ItemSlotComponent from "./ItemSlotComponent";
import {formatTime} from "../game/Util";

@observer
export default class WorkerComponent extends React.Component<WorkerComponentProps, undefined> {

	get worker(): Worker {
		return this.props.worker;
	}

	public render() {
		return (
			<div className="worker-container">
				<div className="panel">
					<div className="mf-column">
						<div>
							<div
								className="worker-recipe-row"
								style={{
									cursor: this.props.onRecipeClick != null ? "pointer" : "initial"
								}}
								onClick={() => this.onRecipeClick()}
							>
								<div className="worker-ingredients-grid">
									{this.worker.recipe != null && this.worker.recipe.items.map((ingredient) =>
										<ItemSlotComponent
											gameData={this.props.game.gameData}
											item={ingredient.item}
											quantity={ingredient.quantity}
											tooltip={false}
											selected={false}
										/>
									)}
								</div>

								<img style={{ width: "22px", height: "21px" }} src="images/PNG/arrowBeige_right.png" />

								{this.worker.recipe != null ? (
									<ItemSlotComponent
										gameData={this.props.game.gameData}
										item={this.props.worker.recipe.result}
										quantity={this.props.worker.recipe.quantity}
										tooltip={false}
										selected={false}
									/>
								) : (
									<ItemSlotComponent
										gameData={this.props.game.gameData}
										item={null}
										quantity={1}
									/>
								)}
							</div>
						</div>
						<div>
							<div style={{width: "100%"}} className={cs({bar: true})}>
								<div className="bar-left-back">
									{this.isWorking() && (
										<div className="bar-left">

										</div>
									)}
								</div>
								<div className="bar-middle-back" style={{position: "relative"}}>
									<div
										className="bar-middle"
										style={{
											width: (this.isWorking()
												? (this.props.worker.task.advancement * 100 / this.props.worker.task.delay)
												: 0
											) + '%',
											textAlign: "center"
										}}>
										{this.isWorking() && (
											<span className="outline" style={{color: "white"}}>
												{formatTime(this.props.worker.task.getTimeRemaining())}
											</span>
										)}
									</div>
									{this.worker.recipe == null && (
										<div className="outline" style={{
											color: "white",
											fontSize: "16px",
											position: "absolute",
											top: 0,
											left: 0,
											textAlign: "center",
											width: "100%",
											cursor: "pointer"
										}}
											 onClick={() => this.onRecipeClick()}
										>
											Choose an item to craft
										</div>
									)}
								</div>
								<div className="bar-right-back">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	onRecipeClick() {
		if (this.props.onRecipeClick != null) {
			this.props.onRecipeClick();
		}
	}

	public isWorking() {
		return this.props.worker.state == 0;
	}

	public isPaused() {
		return this.props.worker.state == 1;
	}

	public isStopped() {
		return this.props.worker.state == 2;
	}
}

interface WorkerComponentProps {
	worker: Worker;
	game: Game;
	onRecipeClick: () => void;
}
