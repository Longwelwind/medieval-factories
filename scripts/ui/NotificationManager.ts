import {AchievementNotification, Notification, PackUnlockedNotification} from "./Notification";
import Game from "../game/Game";
import Pack from "../game/Pack";

export default class NotificationManager {
	/**
	 * The notification manager must be accessed from
	 * multiple classes & components throughtout the code.
	 * There's probably a better way to do that than using a
	 * singleton (such as adding a layer of classes betweent
	 * the components and the game classes) but it works for the moment.
	 */
	static i: NotificationManager;

	game: Game;

	onNotification: (Notification) => void;

	public constructor(game, onNotification: (Notification) => void) {
		if (NotificationManager.i != null) {
			throw new Error("Multiple ");
		}

		NotificationManager.i = this;
		this.game = game;
		this.onNotification = onNotification;

		this.init();
	}

	public init(): void {
		this.game.onBuyPack.addListener((pack: Pack) => {
			this.onPackBuy(pack);
		});

		this.game.onAchievement.addListener((a) => {
			this.popNotification(
				new AchievementNotification(this.game, a)
			);
		})
	}

	public onPackBuy(pack: Pack) {
		this.popNotification(
			new PackUnlockedNotification(this.game, pack)
		);
	}

	public popNotification(notification: Notification) {
		this.onNotification(notification);
	}
}
