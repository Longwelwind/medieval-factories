import GameData from "../game/GameData";
import Item from "../game/Item";
import * as Util from "../game/Util";

import { observer } from "mobx-react";
import * as React from "react";
import * as cs from "react-classset";
import TooltipComponent from "./util/TooltipComponent";
import ItemImageComponent from "./ItemImageComponent";
import {formatTime} from "../game/Util";

@observer
export default class ItemSlotComponent extends React.Component<ItemSlotComponentProps, undefined> {
	static defaultProps: Partial<ItemSlotComponentProps> = {
		tooltip: true,
		selected: false
	};

	public render() {
		let recipe = this.props.gameData.getRecipeForItem(this.props.item);

		return (
			<div className={cs({ slot: true, blue: this.props.selected })}
			>
				{this.props.item != null &&
					<TooltipComponent
						enabled={this.props.tooltip}
						tooltip={
							<div className="tooltip panel beige">
								<div className="mf-row mf-space-between">
									<div>
										<strong>{this.props.item.name}</strong>
									</div>
									<div>
										<div className="money-tag">
											{this.props.item.price}
											<div className="money"></div>
										</div>
									</div>
								</div>
								{recipe != null && recipe.items.length > 0 && (
									<div>
										<div className="mf-row mf-space-between">
											<div>Recipe</div>
											<div>{formatTime(recipe.time)}</div>
										</div>
										<div className="row tight-margin">
											{recipe.items.map((itemQ) => (
											<div className="elem">
												<ItemSlotComponent
													gameData={this.props.gameData}
													item={itemQ.item}
													quantity={itemQ.quantity}
												/>
											</div>
											))}
										</div>
									</div>
								)}
							</div>
						}
					>
						<ItemImageComponent
							x={this.props.item.x} y={this.props.item.y}
							opaque={this.props.quantity > 0}
							draggable={true}
						>
							{this.props.quantity > 1 && (
								<span className="item-quantity outline">
									{Util.formatQuantity(this.props.quantity)}
								</span>
							)}
						</ItemImageComponent>
					</TooltipComponent>
				}
			</div>
		);
	}

	public getItemBackgroundPosition(item: Item) {
		return (-item.x * 32) + "px " + (-item.y * 32) + "px";
	}
}

class ItemSlotComponentProps {
	item: Item;
	gameData: GameData;
	quantity: number;
	selected?: boolean;
	tooltip?: boolean;
}