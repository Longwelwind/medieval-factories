import Item from "../game/Item";
import Game from "../game/Game";
import Worker from "../game/Worker";
import * as Util from "../game/Util";

import {observable} from "mobx";
import {observer} from "mobx-react";
import * as React from "react";
import * as cs from "react-classset";

import {Interface, InterfaceProps} from "./Interface";
import ItemSlotComponent from "./ItemSlotComponent";
import WorkerComponent from "./WorkerComponent";
import ModalComponent from "./util/ModalComponent";
import TooltipComponent from "./util/TooltipComponent";
import Price from "./util/Price";

@observer
export default class WorkerInterface extends Interface<InterfaceProps, undefined> {
	WORKERS_ROW_COUNT: number = 4;
	WORKERS_COLUMN_COUNT: number = 4;

	@observable workshopPage: number = 0;

	/**
	 * Null by default, set when the player clicks on a worker to change
	 * the recipe (thus when not equals to null, the modal should display).
	 */
	@observable workerTryingToChangeRecipe: Worker = null;
	@observable chosenItemToProduce: Item = null;

	public constructor(props) {
		super(props);
	}

	public render() {
		return (
			<React.Fragment>
				{this.workerTryingToChangeRecipe != null &&
					<ModalComponent
						requestClose={() => this.workerTryingToChangeRecipe = null}
					>
						<div className="panel column modal" style={{width: "600px"}}>
							Which item should the worker craft ?
							<div className="row column tight-margin">
								{this.props.game.getKnownItems().map((item) =>
									<div className="col-xs"
										 style={{flex: "0 0 45px"}}
										 onClick={() => this.chosenItemToProduce = item}
									>
										<ItemSlotComponent
											gameData={this.props.game.gameData}
											item={item}
											selected={item == this.chosenItemToProduce}
											quantity={1}
											tooltip={true}
										/>
									</div>
								)}
							</div>
							<div className={cs({"row": true, hidden: this.chosenItemToProduce == null})}>
								<div className="col-xs-4">
									<button
										className="button-block"
										onClick={
											() => this.changeRecipe(this.workerTryingToChangeRecipe, this.chosenItemToProduce)
										}
									>
										Change

									</button>
								</div>
								<div className="col-xs-4">
									<button
										className="button-block"
										onClick={() => this.workerTryingToChangeRecipe = null}
									>
										Cancel
									</button>
								</div>
							</div>
						</div>
					</ModalComponent>
				}
				<div className="row column">
					{this.game.workshopsAllowed && (
						<div className="col-xs-12">
							<div className="panel">
								<div className="row center-xs">
									<button
										className="arrow arrow-left"
										onClick={() => this.changeWorkshopPage(-1)}
									/>
									<div style={{width: "30px", textAlign: "center"}}>{this.workshopPage + 1}</div>
									<button
										className="arrow arrow-right"
										onClick={() => this.changeWorkshopPage(1)}
									/>
								</div>
							</div>
						</div>
					)}
					{this.workshopPage < this.props.game.workshops.length ?
						<div className="col-xs-12">
							<div className="mf-column">
								{this.getDisplayedWorkers().length > 0 && (
									<div className="mf-row mf-wrap tight-margin">
										{this.getDisplayedWorkers().map((worker) => (
											<div style={{width: "33.3%"}}>
												{worker != null &&
												<WorkerComponent
													game={this.props.game}
													worker={worker}
													onRecipeClick={() => this.onRecipeClick(worker)}
												/>
												}
											</div>
										))}
									</div>
								)}
								<div>
									{this.props.game.canBuyWorker(this.workshopPage) &&
                                    <button
                                        style={{width: "180px"}}
                                        onClick={() => this.tryBuyWorker()}
                                    >
                                        <div>
                                            Hire a worker
                                        </div>
										{this.getWorkerPrice() != 0 && (
											<Price game={this.game} price={this.getWorkerPrice()} />
										)}
                                    </button>
									}
								</div>
							</div>
						</div>
						:
						<div className="col-xs-12">
							<div className="panel center-content" style={{height: "400px"}}>
								<div className="row middle-xs center-xs" style={{height: "100%"}}>
									<div className="center-content">
										<div className="inner" style={{textAlign: "center"}}>
											Buying a workshop will make room for
											<strong> {this.props.game.maxWorkersPerWorkshop} </strong>
											more workers.<br/>
											It will cost you:
											<div>
												{this.props.game.getWorkshopPrice()} gold
											</div>
										</div>
										<div className="row center-content">
											<button onClick={() => this.tryBuyWorkshop()}>Buy</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					}
				</div>
			</React.Fragment>
		);
	}

	public changeWorkshopPage(pageDelta: number): void {
		var newPage = this.workshopPage + pageDelta;
		if (newPage < 0 || this.props.game.workshops.length + 1 <= newPage) {
			return;
		}

		this.workshopPage = newPage;
	}

	public getDisplayedWorkers(): Worker[] {
		return this.props.game.workshops[this.workshopPage];
	}

	public tryBuyWorkshop() {
		if (this.props.game.buyWorkshop()) {

		} else {

		}
	}

	public getWorkerPrice(): number {
		return this.props.game.gameData.getWorkerPrice(
			this.workshopPage,
			this.game.workshops[this.workshopPage].length
		);
	}

	public tryBuyWorker() {
		this.props.game.buyWorker(this.workshopPage);
	}

	onRecipeClick(worker: Worker) {
		this.workerTryingToChangeRecipe = worker;
	}

	changeRecipe(worker: Worker, item: Item) {
		let recipe = this.game.gameData.getRecipeForItem(item);

		if (recipe != null) {
			worker.changeRecipe(recipe);
		} else {
			console.warn("No recipe was found for `" + item.id + "`");
		}

		this.workerTryingToChangeRecipe = null;
	}
}

interface WorkerInterfaceProps {
	game: Game;
}