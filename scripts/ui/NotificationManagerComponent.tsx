import Game from "../game/Game";
import { Notification } from "./Notification";
import NotificationManager from "./NotificationManager";
import { observable } from "mobx";
import { observer } from "mobx-react";
import * as React from "react";
import NotificationComponent from "./NotificationComponent";

@observer
export default class NotificationManagerComponent extends React.Component<NotificationManagerComponentProps, null> {
	readonly TIME_BEFORE_DISAPPEAR = 3000;
	readonly SHOWN_NOTIFICATIONS_COUNT = 4;

	notificationManager: NotificationManager;
	@observable notifications: Notification[] = [];

	public render() {
		return (
			<div className="notification-container column tight-margin">
				{this.getShownNotifications().map((notification) =>
					<div
						onClick={() => this.removeNotification(notification)}
					>
						<NotificationComponent
							game={this.props.game}
							notification={notification}
						/>
					</div>
				)}
			</div>
		);
	}

	componentDidMount() {
		this.notificationManager = new NotificationManager(
			this.props.game,
			(notification: Notification) => {
				this.addNotification(notification);
		});
	}

	private getShownNotifications(): Notification[] {
		return this.notifications.slice(0, this.SHOWN_NOTIFICATIONS_COUNT);
	}

	private addNotification(notification: Notification): void {
		this.notifications.push(notification);

		// We set the auto-disappear timer for this notification
		setTimeout(() => {
			this.removeNotification(notification);
		}, this.TIME_BEFORE_DISAPPEAR);
	}

	private removeNotification(notification: Notification) {
		let index = this.notifications.indexOf(notification);

		if (index != -1) {
			this.notifications.splice(index, 1);
		}
	}
}

interface NotificationManagerComponentProps {
	game: Game
}