import GameData from "./../game/GameData";
import ItemQuantity from "../game/ItemQuantity";

import { observer } from "mobx-react";
import * as React from "react";
import * as cs from "react-classset";

import ItemSlotComponent from "./ItemSlotComponent";

@observer
export default class BlankItemSlotComponent extends React.Component<BlankItemSlotComponentProps, undefined> {
	public render() {
		return (
			<ItemSlotComponent
				gameData={this.props.gameData}
				item={this.props.itemQuantity != null ? this.props.itemQuantity.item : null}
				selected={this.props.selected}
				quantity={this.props.itemQuantity != null ? this.props.itemQuantity.quantity : 0}
				tooltip={true}
			/>
		);
	}
}

class BlankItemSlotComponentProps {
	gameData: GameData;
	itemQuantity: ItemQuantity;
	selected: boolean;
	tooltip: boolean;
}
