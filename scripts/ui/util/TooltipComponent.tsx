import * as React from "react"
import { observer } from "mobx-react";
import { observable } from "mobx";
import * as cs from "react-classset";
import {createPortal} from "react-dom";

/**
 * This class requires that a div with the id `tooltip-container` exists in the DOM tree.
 * This allows tooltips to be placed on top of the DOM tree (using React's portals)
 */
@observer
export default class TooltipComponent extends React.Component<TooltipComponentProps, null> {
	public static defaultProps: Partial<TooltipComponentProps> = {
		enabled: true,
		sideToSide: false
	};

	readonly OFFSET = 10;

	tooltipContainer: HTMLSpanElement;
	@observable hovered: boolean = false;
	@observable x: number = 0;
	@observable y: number = 0;

	public render() {
		let tooltipContainer = document.getElementById("tooltip-container");
		if (tooltipContainer == null) {
			throw new Error("No DOM node with id `tooltip-container` exist");
		}

		return (
			<div
				style={{position: "relative"}}
				onMouseEnter={(e) => this.onMouseEnter(e)}
				onMouseMove={(e) => this.onMouseMove(e)}
				onMouseLeave={(e) => this.onMouseLeave(e)}
				ref={(e) => this.tooltipContainer = e}
			>
				{this.hovered && (
					createPortal(
						<div
							style={{
								position: "absolute",
								left: this.x + this.OFFSET,
								top: this.y + this.OFFSET,
								zIndex: 1000,
							}}
						>
							{this.props.tooltip}
						</div>,
						tooltipContainer
					)
				)}
				{this.props.children}
			</div>
		);
	}

	public onMouseEnter(e: React.MouseEvent<HTMLDivElement>): void {
		if (this.props.enabled) {
			this.hovered = true;
			this.x = e.pageX;
			this.y = e.pageY;
		}
	}

	private onMouseMove(e: React.MouseEvent<HTMLDivElement>): void {
		if (this.hovered) {
			this.x = e.pageX;
			this.y = e.pageY;
		}
	}

	public onMouseLeave(e: React.MouseEvent<HTMLDivElement>): void {
		this.hovered = false;
	}
}

interface TooltipComponentProps {
	tooltip: JSX.Element;
	enabled?: boolean;
	sideToSide?: boolean;
}