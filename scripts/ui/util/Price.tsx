import {Component} from "react";
import * as React from "react";
import Game from "../../game/Game";
import {observer} from "mobx-react";

interface PriceProps {
	game: Game,
	price: number
}

@observer
export default class Price extends Component<PriceProps> {
	render() {
		return (
			<div className="money-tag center-content">
				<span
					style={{color: this.props.game.money < this.props.price ? "#e69667" : "inherit"}}
				>
					<b>{this.props.price}</b>
				</span>
				<div className="money" />
			</div>
		);
	}
}