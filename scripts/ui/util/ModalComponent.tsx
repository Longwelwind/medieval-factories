import * as React from "react";

interface ModalComponentProps {
	requestClose?: () => void;
}

export default class ModalComponent extends React.Component<ModalComponentProps, null> {
	public defaultProps: Partial<ModalComponentProps> = {
		requestClose: () => {}
	};

	public render() {
		return (
			<div className="modal-backdrop" onClick={() => { this.props.requestClose() }}>
				<div className="modal-container" onClick={(e) => { e.stopPropagation() }}>
					{this.props.children}
				</div>
			</div>
		);
	}
}