import { Notification } from "./Notification";
import ItemSlotComponent from "./ItemSlotComponent";
import Game from "../game/Game";
import { observable } from "mobx";
import { observer } from "mobx-react";
import * as React from "react";
import ItemImageComponent from "./ItemImageComponent";

@observer
export default class NotificationComponent extends React.Component<NotificationComponentProps, null> {
	public render() {
		let icon = this.props.notification.getItemIcon();
		return (
			<div className="panel notification" style={{fontSize: "15px"}}>
				<div className="mf-row small-margin mf-align-center">
					{icon != null && (
						<div>
							<div className="slot">
								<ItemImageComponent x={icon.x} y={icon.y} />
							</div>
						</div>
					)}
					<div className="notification-text">
						{this.props.notification.getText()}
					</div>
				</div>
			</div>
		);
	}
}

interface NotificationComponentProps {
	game: Game;
	notification: Notification;
}
