import Pack from "../game/Pack";
import * as Util from "../game/Util";
import Game from "../game/Game";

import { Interface, InterfaceProps } from "./Interface";
import ItemSlotComponent from "./ItemSlotComponent";
import TooltipComponent from "./util/TooltipComponent";
import { NotEnoughMoneyNotification } from "./Notification";
import NotificationManager from "./NotificationManager";

import { observable } from "mobx";
import { observer } from "mobx-react";
import * as React from "react";
import * as cs from "react-classset";
import ItemImageComponent from "./ItemImageComponent";
import PackTree from "../game/PackTree";
import {Fragment} from "react";
import Price from "./util/Price";

@observer
export default class ItemsInterface extends Interface<InterfaceProps, undefined> {
	canvas: HTMLCanvasElement;

	@observable selectedPackTree = this.props.game.gameData.packTrees[0];

	public render() {
		return (
			<div className="mf-column">
				<div>
					<div className="panel">
						<div className="mf-column">
							<div>
								<h2>Research tree</h2>
							</div>
							<div className="mf-row">
								{this.props.game.gameData.packTrees.map(packTree => (
									<div>
										<button
											className={cs({blue: this.selectedPackTree == packTree})}
											onClick={() => this.setSelectedPackTree(packTree)}
											disabled={
												this.selectedPackTree == packTree
												|| !this.game.isPackTreeUnlocked(packTree)
											}
											style={{
												opacity: this.game.isPackTreeUnlocked(packTree) ? 1 : 0.4
											}}
										>
											<ItemImageComponent
												x={this.game.isPackTreeUnlocked(packTree)
													? packTree.x
													: Game.QUESTION_MARK_X}
												y={this.game.isPackTreeUnlocked(packTree)
													? packTree.y
													: Game.QUESTION_MARK_Y}
											/>
										</button>
									</div>
								))}
							</div>
						</div>
					</div>
				</div>
				<div className="mf-row mf-center">
					<div>
						<div className="panel">
							<div className="mf-column mf-center">
								<div>
									<h2>{this.selectedPackTree.name}</h2>
								</div>
								<div>
									<div className="items-tree inner"
									>
										<canvas
											id="backdrop"
											style={{position: "absolute"}}
											width={this.game.gameData.getWidthPackTree()}
											height={this.game.gameData.getHeightPackTree()}
											ref={(c) => this.canvas = c}
										>
										</canvas>
										<div style={{
											width: (this.game.gameData.getWidthPackTree()) + 'px',
											height: (this.game.gameData.getHeightPackTree()) + 'px',
											position: "relative"
										}}>
											{this.selectedPackTree.packs.map((pack) => (
												<div
													style={{
														position: "absolute",
														left: (pack.x + this.game.gameData.getWidthPackTree() / 2 - 22.5) + 'px',
														top: (pack.y - 22.5) + 'px'
													}}
													onClick={() => this.buyPack(pack)}
												>
													{this.props.game.isPackBought(pack) || this.props.game.isPackBuyable(pack) ? (
														<TooltipComponent
															sideToSide={true}
															tooltip={
																<div
																	className="tooltip panel"
																	style={{ width: "320px" }}
																>
																	<div className="mf-row mf-space-between mf-align-center">
																		<div>
																			<h4>{pack.name}</h4>
																		</div>
																		<div>
																			{!this.props.game.isPackBought(pack) && (
																				<Price game={this.props.game} price={pack.price} />
																			)}
																		</div>
																	</div>
																	<div dangerouslySetInnerHTML={{__html: pack.description}}></div>
																	{pack.displayNumberWorkshopInTooltip && pack.maxNumberWorkersPerWorkshop > 0 && (
																		<div>
																			Allows for <b>{pack.maxNumberWorkersPerWorkshop}</b> worker in a workshop
																		</div>
																	)}
																	{pack.recipes.length > 0 && (
																		<div>
																			<div>
																				Unlocks:
																			</div>
																			<div className="row column tight-margin">
																				{pack.recipes.map((recipe) => (
																					<div
																						className="col-xs"
																						style={{ flex: "0 0 45px" }}
																					>
																						<ItemSlotComponent
																							gameData={this.props.game.gameData}
																							item={recipe.result}
																							quantity={1}
																							tooltip={true}
																							selected={false}
																						/>
																					</div>
																				))}
																			</div>
																		</div>
																	)}
																</div>
															}
														>
															<div
																className={cs({slot: true, blue: this.props.game.isPackBought(pack)})}
																style={{
																	cursor: this.props.game.isPackBuyable(pack) ? "pointer" : "initial"
																}}
															>
																<ItemImageComponent
																	x={pack.iconX} y={pack.iconY}
																/>
															</div>
														</TooltipComponent>
													) : (
														<div className="slot">
															<div className="item question-mark">
															</div>
														</div>
													)}
												</div>
											))}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	public buyPack(pack: Pack) {
		if (!this.props.game.isPackBuyable(pack)) {
			return;
		}

		if (this.props.game.buyPack(pack)) {

		} else {
			NotificationManager.i.popNotification(
				new NotEnoughMoneyNotification(this.props.game)
			);
		}
	}

	public componentDidMount() {
		this.drawPrerequisiteLines();
	}

	setSelectedPackTree(packTree: PackTree) {
		this.selectedPackTree = packTree;

		this.drawPrerequisiteLines();
	}

	private drawPrerequisiteLines() {
		const NORMAL_LINK_COLOR = "#B7916A";
		const BOUGHT_LINK_COLOR = "#727685";

		let ctx = this.canvas.getContext("2d");
		ctx.clearRect(0, 0, this.canvas.clientWidth, this.canvas.clientHeight);

		ctx.lineWidth = 2;

		this.selectedPackTree.packs.forEach((p) => {
			p.prerequisites.forEach((preq) => {
				ctx.strokeStyle = this.game.isPackBought(p) && this.game.isPackBought(preq)
					? BOUGHT_LINK_COLOR
					: NORMAL_LINK_COLOR;
				ctx.beginPath();
				// Since the tree is x-centered, we must center the links too
				let centerX = this.game.gameData.getWidthPackTree() / 2;
				ctx.moveTo(centerX + p.x, p.y);
				ctx.lineTo(centerX + preq.x, preq.y);
				ctx.stroke();
			});
		});
	}

	public getMaxHeight(): number {
		let max = 0;

		this.props.game.gameData.packs.forEach((p) => {
			max = Math.max(max, p.y);
		});

		return max;
	}
}
