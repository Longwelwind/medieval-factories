import Game from "../game/Game";
import Item from "../game/Item";
import Worker from "../game/Worker";
import Recipe from "../game/Recipe";
import Pack from "../game/Pack";
import * as Util from "../game/Util";
import ItemQuantity from "../game/ItemQuantity";

import { observable, toJS } from "mobx";
import { observer } from "mobx-react";
import * as React from "react";
import * as cs from "react-classset";
import { Serializer } from "../serialization/Serialization";

import { Interface, InterfaceProps } from "./Interface";
import WorkerInterface from "./WorkerInterface";
import ItemsInterface from "./ItemsInterface";
import OptionsInterface from "./OptionsInterface";
import BlankItemSlot from "./BlankItemSlotComponent";
import TooltipComponent from "./util/TooltipComponent";
import NotificationManagerComponent from "./NotificationManagerComponent";
import ItemImageComponent from "./ItemImageComponent";
import CraftInterface from "./CraftInterface";
import AchievementInterface from "./AchievementInterface";

@observer
export default class GameApp extends React.Component<GameAppProps, undefined> {
	readonly ITEMS_PER_PAGE = 24;

	@observable currentInterface: typeof Interface = WorkerInterface;
	@observable inventoryPage: number = 0;
	@observable selectedItem: Item = null;

	public render() {
		return (
			<div style={{ position: "relative" }}>
				<div id="game-container" className="mf-column">
					<div className="mf-row">
						{/*<div>
							<button onClick={() => this.setCurrentInteface(CraftInterface)}>
								<ItemImageComponent x={3} y={101}/>
							</button>
						</div>*/}
						<div>
							<button onClick={() => this.setCurrentInteface(WorkerInterface)}>
								<ItemImageComponent x={2} y={101}/>
							</button>
						</div>
						<div>
							<button onClick={() => this.setCurrentInteface(ItemsInterface)}>
								<ItemImageComponent x={2} y={61}/>
							</button>
						</div>
						<div style={{flex: 1}} />
						<div>
							<button onClick={() => this.setCurrentInteface(AchievementInterface)}>
								<ItemImageComponent x={13} y={2}/>
							</button>
						</div>
						<div>
							<button onClick={() => this.setCurrentInteface(OptionsInterface)}>
								<ItemImageComponent x={11} y={15}/>
							</button>
						</div>
					</div>
					<div className="row">
						<div className="col-xs-12 col-sm col-inventory">
							<div className="column">
								<div>
									<div className="panel">
										<div className="column">
											<div>
												<h2>Warehouse</h2>
											</div>
											{this.getMaxInventoryPage() > 0 &&
												<div className="row center-content">
													<button
														style={{ visibility: this.inventoryPage != 0 ? "initial" : "hidden" }}
														onClick={() => this.changeInventoryPage(-1)}
														className="arrow arrow-left"
													/>
													<div style={{ minWidth: "30px", textAlign: "center" }}>
														{this.inventoryPage + 1}
													</div>
													<button
														style={{ visibility: this.inventoryPage != this.getMaxInventoryPage() ? "initial" : "hidden" }}
														onClick={() => this.changeInventoryPage(1)}
														className="arrow arrow-right"
													/>
												</div>
											}
											<div className="row column between-xs tight-margin">
												{this.getDisplayedItems().map((entry, i) =>
													<div
														key={i}
														className="col-xs"
														style={{ flex: "0 0 45px" }}
														onClick={() => this.selectItemInventory(entry.item)}
													>
														<BlankItemSlot
															gameData={this.props.game.gameData}
															itemQuantity={entry}
															selected={entry != null && this.selectedItem == entry.item}
															tooltip={true}
														/>
													</div>
												)}
											</div>
											<div
												className={cs({
													"mf-row": true,
													"mf-align-center": true,
													hidden: this.selectedItem == null
												})}
												style={{ justifyContent: "space-between" }}
											>
												<div>
													<button
														className="button-block"
														onClick={() => this.props.game.sellItem(this.selectedItem, 1)}
													>
														Sell
													</button>
												</div>
												<div>
													<TooltipComponent
														tooltip={
															<div className="panel tooltip" style={{ width: "180px", textAlign: "left" }}>
																<strong>Auto-sell</strong>
																<div>
																	Items will be sold as soon as it is produced for maximum profit
																</div>
															</div>
														}
													>
														<div>
															<input type="checkbox" className="mf-checkbox" checked={this.props.game.isAutosell(this.selectedItem)} onChange={() => this.onAutosellChange()} id="cb"></input>
															<label htmlFor="cb"></label>
														</div>
													</TooltipComponent>
												</div>
											</div>
											<div>
												<div className="inner" id="money-container">
													{this.props.game.money}
													<div className="money"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="col-xs">
							<this.currentInterface
								game={this.props.game}
								selectedItem={this.selectedItem}
							/>
						</div>
					</div>
				</div>
				<NotificationManagerComponent game={this.props.game} />
			</div>
		);
	}

	public setCurrentInteface(selInterface: typeof Interface) {
		this.currentInterface = selInterface;
	}

	/**
	 * Inventory panel
	 */
	public selectItemInventory(item: Item) {
		if (item == null) {
			return;
		}

		if (this.selectedItem == item) {
			this.selectedItem = null;
		} else {
			this.selectedItem = item;
		}
	}

	public getDisplayedItems(): ItemQuantity[] {
		let indexBegin = this.inventoryPage * this.ITEMS_PER_PAGE;
		let displayedItems = this.props.game.inventory.items.slice(indexBegin, indexBegin + this.ITEMS_PER_PAGE);

		// We fill the array with null to have a size of ITEMS_PER_PAGE
		while (displayedItems.length < this.ITEMS_PER_PAGE) {
			displayedItems.push(null);
		}

		return displayedItems;
	}

	public changeInventoryPage(delta: number) {
		this.inventoryPage = Math.max(0, Math.min(this.inventoryPage + delta, this.getMaxInventoryPage()));
	}

	public getMaxInventoryPage() {
		return Math.floor(this.props.game.inventory.items.length / this.ITEMS_PER_PAGE);
	}

	public onAutosellChange() {
		this.props.game.setAutosellItem(this.selectedItem, !this.props.game.isAutosell(this.selectedItem));
	}
}

interface GameAppProps {
	game: Game;
}
