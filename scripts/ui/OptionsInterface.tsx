import { Interface, InterfaceProps } from "./Interface";

import { observable } from "mobx";
import { observer } from "mobx-react";
import * as React from "react";
import * as cs from "react-classset";

@observer
export default class OptionsInterface extends Interface<InterfaceProps, null> {
	public render() {
		let lastSaveDate = new Date(this.game.lastSaveTime);

		return (
			<div className="mf-column">
				<div className="mf-row mf-center">
					<div>
						<div className="panel">
							<h2>Options</h2>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-12 col-md-6">
						{this.game.saveUnlocked && (
							<div className="panel">
								<div className="mf-column small-margin">
									<div style={{marginLeft: "40px", marginRight: "40px"}}>
										<button style={{width: "100%"}} onClick={() => this.game.save()}>Save</button>
									</div>
									<div style={{display: "flex", alignItems: "center", flexWrap: "wrap"}}>
										<div>
											<input
												id="autosave"
												className="mf-checkbox"
												type="checkbox"
												checked={this.game.autoSaveEnabled}
												onChange={() => this.game.autoSaveEnabled = !this.game.autoSaveEnabled}
											/>
											<label htmlFor="autosave"></label>
										</div>
										<div style={{marginLeft: "0.4em"}}>Auto save</div>
										<div style={{flexGrow: 1}}></div>
										<div style={{fontSize: "0.9em"}}>
											Last save: {lastSaveDate.getHours()}
											:{lastSaveDate.getMinutes()}
											:{lastSaveDate.getSeconds()}
										</div>
									</div>
								</div>
							</div>
						)}
					</div>
					<div className="col-xs-12 col-md-6 mf-column">
						{env.DEBUG && (
							<div>
								<div className="panel">
									<h2>Dev console</h2>

									<div className="row column">
										<div className="col-xs-12">
											<button onClick={() => this.giveMoney()}>+1000 Money</button>
										</div>
										<div className="col-xs-12">
											<button onClick={() => this.randomUnknownItem()}>Random unknown item</button>
										</div>
										<div className="col-xs-12">
											<button onClick={() => this.advanceGame(60*50)}>
												Advance time for 50 minutes
											</button>
										</div>
									</div>
								</div>
							</div>
						)}
						<div>
							<div className="panel">
								<div className="mf-column">
									<div>
										<h2>About</h2>
									</div>
									<div>
										<strong>Medieval Factories v{this.game.version}</strong> is an free open-source game developed
										by <a href="https://twitter.com/Longwelwind">@Longwelwind</a>, available
										on <a href="https://github.com/Longwelwind/medieval-factories">Github</a>.
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	public giveMoney() {
		this.props.game.addMoney(1000);
	}

	public randomUnknownItem() {
		let unknownItems = this.props.game.gameData.items.filter((i) => !this.props.game.isItemKnown(i));
		if (unknownItems.length == 0) {
			return;
		}

		let unknownItem = unknownItems[Math.floor(Math.random() * unknownItems.length)];
		this.props.game.inventory.addItem(unknownItem, 1);
	}

	advanceGame(seconds: number) {
		this.game.update(seconds);
	}
}