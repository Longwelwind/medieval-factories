import * as React from "react";

import Game from "../game/Game";
import Item from "../game/Item";

export class Interface<Props extends InterfaceProps = InterfaceProps, State = any>
	extends React.Component<Props, State>
{
	get game(): Game {
		return this.props.game;
	}
}

export interface InterfaceProps {
	game: Game;
	selectedItem: Item;
}