import {Interface} from "./Interface";
import {ReactNode} from "react";
import * as React from "react";
import {observable} from "mobx";
import Item from "../game/Item";
import {observer} from "mobx-react";
import ItemSlotComponent from "./ItemSlotComponent";
import Recipe from "../game/Recipe";
import * as cs from "react-classset";

@observer
export default class CraftInterface extends Interface {
	@observable selectedItem: Item;

	render(): ReactNode {
		let recipe: Recipe = null;
		if (this.selectedItem != null) {
			recipe = this.game.gameData.getRecipeForItem(this.selectedItem);
		}

		let hasIngredientsToCraft = false;
		if (recipe != null) {
			hasIngredientsToCraft = this.game.hasIngredientsToCraft(recipe);
		}

		return (
			<div className="mf-column mf-align-center">
				<div style={{alignSelf: "stretch"}}>
					<div className="panel">
						<div className="mf-column">
							<div>
								Click on an item to craft
							</div>
							<div>
								<div className="mf-row mf-wrap tight-margin">
									{this.game.getKnownItems().map(item => (
										<div onClick={() => this.selectedItem = item}>
											<ItemSlotComponent
												gameData={this.props.game.gameData}
												item={item}
												quantity={1}
												selected={this.selectedItem == item}
											/>
										</div>
									))}
								</div>
							</div>
						</div>
					</div>
				</div>
				{this.selectedItem != null && recipe != null && (
					<div>
						<div className="panel">
							<div className="mf-column mf-align-center">
								<div className="mf-row mf-center">
									<div className="mf-row tight-margin" style={{width: "180px"}}>
										{recipe.items.map(iq => (
											<div>
												<ItemSlotComponent
													gameData={this.props.game.gameData}
													item={iq.item}
													quantity={iq.quantity}
													tooltip={false}
												/>
											</div>
										))}
									</div>
									<div className="mf-row mf-center mf-align-center">
										<div>
											<img style={{ width: "28px", height: "20px" }} src="images/PNG/arrowBrown_right.png" />
										</div>
									</div>
									<div className="mf-row tight-margin">
										<div>
											<ItemSlotComponent
												gameData={this.props.game.gameData}
												item={recipe.result}
												quantity={recipe.quantity}
												tooltip={false}
											/>
										</div>
									</div>
								</div>
								<div style={{alignSelf: "stretch"}}>
									<div className="bar">
										<div className="bar-left-back">
											{this.game.currentlyCraftingTask != null && (
												<div className="bar-left" />
											)}
										</div>
										<div className="bar-middle-back">
											{this.game.currentlyCraftingTask != null && (
												<div
													className="bar-middle"
													style={{
														width: (this.game.currentlyCraftingTask.advancement * 100 / this.game.currentlyCraftingTask.delay) + '%'
													}}
												/>
											)}
										</div>
										<div className="bar-right-back">
										</div>
									</div>
								</div>
								<div>
									<button
										className={cs({blue: !hasIngredientsToCraft})}
										style={{
											minWidth: "180px",
											visibility: this.game.currentlyCraftingTask == null ? "initial" : "hidden"
										}}
										onClick={() => this.game.craft(recipe)}
										disabled={!hasIngredientsToCraft}
									>
										{hasIngredientsToCraft ? (
											"Craft"
										) : (
											"Not enough materials"
										)}
									</button>
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}