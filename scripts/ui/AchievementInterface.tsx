import {Interface} from "./Interface";
import {ReactNode} from "react";
import * as React from "react";
import ItemImageComponent from "./ItemImageComponent";
import {MoneyReachedCondition} from "../game/achievement/Condition";

export default class AchievementInterface extends Interface {
	render(): ReactNode {
		return (
			<div className="mf-column">
				<div className="mf-row mf-center">
					<div>
						<div className="panel">
							<h2>Achievements</h2>
						</div>
					</div>
				</div>
				<div className="mf-row mf-wrap">
					{this.game.gameData.achievements.map(a => (
						<div style={{width: "50%"}}>
							<div className="panel">
								<div className="mf-row">
									<div>
										<div className="slot">
											<div style={{opacity: this.game.isAccomplished(a) ? 1 : 0.2}}>
												<ItemImageComponent x={a.iconX} y={a.iconY}/>
											</div>
										</div>
									</div>
									<div style={{flexGrow: 1}}>
										<div style={{opacity: this.game.isAccomplished(a) ? 1 : 0.4}}>
											<div style={{textAlign: "center"}}>
												<h4>{a.name}</h4>
											</div>
											<div style={{textAlign: "center"}}>
											{a.condition instanceof MoneyReachedCondition ? (
												<span>Own <b>{a.condition.amount}</b> gold</span>
											) : (
												"[!] The developer hasn't coded this condition yet"
											)}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		);
	}
}