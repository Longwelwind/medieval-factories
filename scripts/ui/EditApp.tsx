import GameData from "../game/GameData";
import Item from "../game/Item";
import ItemQuantity from "../game/ItemQuantity";
import Recipe from "../game/Recipe";
import ItemSlotComponent from "./ItemSlotComponent";
import Pack from "../game/Pack";
import * as Util from "../game/Util";
import { observer } from "mobx-react";
import { observable } from "mobx";
import * as React from "react";
import * as cs from "react-classset";
import PackTree from "../game/PackTree";
import {Component, ReactNode} from "react";
import ItemImageComponent from "./ItemImageComponent";
import TooltipComponent from "./util/TooltipComponent";
import {Serializer} from "../serialization/Serialization";
import createGameSerializer from "../createGameSerializer";
import Timer = NodeJS.Timer;
import {formatTime} from "../game/Util";
import Condition, {MoneyReachedCondition, PackBoughtCondition} from "../game/achievement/Condition";
import Achievement from "../game/achievement/Achievement";

interface EditAppProps {
	gameData: GameData;
}

const conditionsForAchievements: [string, (g: GameData, a: Achievement) => Condition][] = [
	[
		PackBoughtCondition.name,
		(g, a) => {
			if (g.packs.length == 0) {
				alert("A pack must first be created");
				return null;
			}
			return new PackBoughtCondition(a, g.packs[0]);
		}
	],
	[
		MoneyReachedCondition.name,
		(g, a) => {
			return new MoneyReachedCondition(a);
		}
	]
];

@observer
export default class EditApp extends React.Component<EditAppProps, null> {

	@observable selectedItem: Item = null;
	@observable shownItem: Item = null;
	@observable shownPack: Pack = null;
	@observable shownPackTree: PackTree = null;
	@observable selectedPackPrerequisite: Pack = this.props.gameData.packs[0];
	@observable shownAchievement: Achievement = null;

	@observable jsonSave: string = "";

	@observable newItemText: string = "Id";
	@observable newPackTreeText: string = "Id";
	@observable newPackText: string = "Id";

	@observable lastTimeSaved: Date;
	saveIntervalId: Timer;

	get gameData(): GameData {
		return this.props.gameData;
	}

	public render() {
		return (
			<div className="mf-row">
				<div className="grid-column" style={{ width: "200px" }}>
					<div className="row column tight-margin">
						{this.props.gameData.items.map((item) => (
							<div
								className="col-xs"
								style={{ flex: "0 0 45px" }}
								onClick={() => this.selectedItem = item != this.selectedItem ? item : null}
								onDoubleClick={() => this.shownItem = item != this.shownItem ? item : null}
							>
								<ItemSlotComponent
									gameData={this.props.gameData}
									item={item}
									quantity={1}
									selected={this.selectedItem == item}
								/>
							</div>

						))}
					</div>
					<div>
						<input
							value={this.newItemText}
							onChange={(e) => this.newItemText = e.target.value}
						/>
					</div>
					<div>
						<button onClick={() => this.addItem()}>Add an Item</button>
					</div>
					<div className="grid-column">
						{this.props.gameData.packTrees.map((packTree) => (
							<div
								className={cs({panel: true, blue: this.shownPackTree == packTree})}
								onClick={() => this.shownPackTree = packTree}
							>
								{packTree.name}
							</div>
						))}
					</div>
					<div>
						<input value={this.newPackTreeText} onChange={(e) => this.newPackTreeText = e.target.value} />
					</div>
					<div>
						<button onClick={() => this.addPackTree()}>Add a Pack Tree</button>
					</div>
					<div>
						{this.props.gameData.achievements.map(a => (
							<div>
								<div
									className={cs({panel: true, blue: this.shownAchievement == a})}
									onClick={() => this.shownAchievement = a}
								>
									{a.name}
								</div>
							</div>
						))}
						<div>
							<button onClick={() => this.addAchievement()}>Add an Achievement</button>
						</div>
					</div>
					<div>
						Starting packs:
						{this.gameData.startingPacks.map((pr, i) => (
							<div onClick={() => this.gameData.startingPacks.splice(i, 1)}>
								{pr.name}
							</div>
						))}
					</div>
					<div>
						<textarea value={this.jsonSave} /><br />
						{this.lastTimeSaved != null && (
							<div>
								Saved at {this.lastTimeSaved.getHours()}
									:{this.lastTimeSaved.getMinutes()}
									:{this.lastTimeSaved.getSeconds()}
							</div>
						)}
						<button onClick={() => this.refreshJSONSave()}>Refresh save</button>
					</div>
				</div>
				{
					/**
					 * "Modifying an item" section
					 */
				}
				<div className="mf-column">
					{this.shownItem != null && (
						<div>
							<ItemSlotComponent
								gameData={this.props.gameData}
								item={this.shownItem}
								quantity={1}
							/>
							<button onClick={() => this.removeItem(this.shownItem)}>Remove item</button>
							<br />
							Id:
							<input
								value={this.shownItem.id}
								onChange={(e) => this.shownItem.id = e.currentTarget.value}
							/><br />
							Name:
							<input
								value={this.shownItem.name}
								onChange={(e) => this.shownItem.name = e.currentTarget.value}
							/><br />
							Price
							<input
								value={this.shownItem.price}
								onChange={(e) => this.shownItem.price = parseInt(e.currentTarget.value)}
							/><br />

							Recipe:
						{this.props.gameData.hasRecipe(this.shownItem) ? (
								<div>
									Quantity:
									<input
										type="number"
										value={this.props.gameData.getRecipeForItem(this.shownItem).quantity}
										onChange={
											(e) => this.props.gameData.getRecipeForItem(this.shownItem).quantity = parseInt(e.currentTarget.value)
										}
									/><br />
									Time:
									<input
										type="number"
										value={this.props.gameData.getRecipeForItem(this.shownItem).time}
										onChange={
											(e) => this.props.gameData.getRecipeForItem(this.shownItem).time = parseInt(e.currentTarget.value)
										}
									/><br />
									<div className="row">
										{this.props.gameData.getRecipeForItem(this.shownItem).items.map((itemQ) => (
											<div>
												<div className="row" onClick={() => this.removeItemRecipe(this.shownItem, itemQ)}>
													<ItemSlotComponent
														gameData={this.props.gameData}
														item={itemQ.item}
														quantity={itemQ.quantity}
													/>
												</div>
												<div className="column center-content">
													<input
														type="number"
														style={{ width: "40px" }}
														value={itemQ.quantity}
														onChange={(e) => itemQ.quantity = parseInt(e.currentTarget.value)}
													/>
												</div>
											</div>
										))}
										<button onClick={() => this.addItemRecipe(this.shownItem, this.selectedItem)}>
											Add item ingredients
										</button>
									</div>
									<div>
										<button onClick={() => this.removeRecipe(this.shownItem)}>
											Remove the recipe
									</button>
									</div>
									Rentability: {this.computeRentability(this.shownItem)} [g/s]<br/>
									Gained rentability: {this.computeGainedRentability(this.shownItem)} [g/s]
								</div>
							) : (
									<div>
										<button onClick={() => this.addRecipe(this.shownItem)}>Add recipe</button>
									</div>
								)}
							Image:<br />
							<IconPickerComponent
								onIconClick={(x, y) => { this.shownItem.x = x; this.shownItem.y = y }}
							/>
						</div>
					)}
				</div>
				{
					/**
					 * Modifying a pack tree section
					 */
				}
				{this.shownPackTree != null && (
					<div className="mf-column">
						<div>
							<ItemImageComponent
								x={this.shownPackTree.x} y={this.shownPackTree.y}
							/>
							Id:
							<input
								value={this.shownPackTree.id}
								onChange={(e) => this.shownPackTree.id = e.currentTarget.value}
							/><br />
							Name:
							<input
								value={this.shownPackTree.name}
								onChange={(e) => this.shownPackTree.name = e.currentTarget.value}
							/><br />
							<div>
								Pre-Unlocked: <input
									type="checkbox"
									checked={this.shownPackTree.preUnlocked}
									onChange={() => this.shownPackTree.preUnlocked = !this.shownPackTree.preUnlocked}
								/>
							</div>
							<div className="grid-column">
								{this.shownPackTree.packs.map((pack) => (
									<div className="panel" onClick={() => this.shownPack = pack}>
										{pack.name}
									</div>
								))}
							</div>
							<div>
								<input value={this.newPackText} onChange={(e) => this.newPackText = e.target.value} />
							</div>
							<div>
								<button onClick={() => this.addPack()}>Add a pack</button>
							</div>
							<div>
								<div className="panel">
									<div className="mf-column mf-align-center">
										<div>I</div>
									</div>
									<div className="inner">
										<div
											style={{
												position: "relative",
												width: this.gameData.getWidthPackTree() + 'px',
												height: this.gameData.getHeightPackTree() + 'px',
											}}
											ref={(e) => this.packsContainer = e}
											onClick={(e) => this.onPackBackdropClick(e)}
										>
										{this.shownPackTree.packs.map((pack) => (
											<div
												style={{
													position: "absolute",
													left: (pack.x + this.gameData.getWidthPackTree() / 2 - 22.5) + 'px',
													top: (pack.y - 22.5) + 'px'
												}}
												onClick={() => this.shownPack = pack}
											>
												<div className={cs({slot: true, blue: this.shownPack == pack})}>
													<ItemImageComponent
														x={pack.iconX} y={pack.iconY}
													/>
												</div>
											</div>
										))}
										</div>
									</div>
								</div>
							</div>
							Image:<br />
							<IconPickerComponent
								onIconClick={(x, y) => { this.shownPackTree.x = x; this.shownPackTree.y = y }}
							/>
							<div>
							</div>
						</div>
					</div>
				)}
				{
					/**
					 * "Modifying a pack" section
					 */
				}
				{this.shownPack != null && (
					<div className="mf-column">
						<ItemImageComponent
							x={this.shownPack.iconX} y={this.shownPack.iconY}
						/>
						<button onClick={() => this.deletePack()}>Delete</button><br />
						<div>
							{this.gameData.startingPacks.indexOf(this.shownPack) == -1 && (
								<button
									onClick={() => this.gameData.startingPacks.push(this.shownPack)}
								>
									Add as starting pack
								</button>
							)}
						</div>
						Id:
						<input
							value={this.shownPack.id}
							onChange={(e) => this.shownPack.id = e.currentTarget.value}
						/><br />
						Name:
						<input
							value={this.shownPack.name}
							onChange={(e) => this.shownPack.name = e.currentTarget.value}
						/>
						<div>
							Description:<br/>
							<textarea
								value={this.shownPack.description}
								onChange={e => this.shownPack.description = e.target.value}
							/>
						</div>
						X:
						<input
							value={this.shownPack.x}
							onChange={(e) => this.shownPack.x = parseInt(e.currentTarget.value)}
						/>
						Y:
						<input
							value={this.shownPack.y}
							onChange={(e) => this.shownPack.y = parseInt(e.currentTarget.value)}
						/><br />
						<div>
							Max number of workers per workshops
							<input
								value={this.shownPack.maxNumberWorkersPerWorkshop}
								onChange={
									(e) => this.shownPack.maxNumberWorkersPerWorkshop = parseInt(e.currentTarget.value)
								}
							/>
						</div>
						<div>
							Unlocked Pack Trees
							{this.gameData.packTrees.map(pt => (
								<div>
									<input
										type="checkbox"
										checked={this.shownPack.unlockedPackTrees.indexOf(pt) != -1}
										onChange={() => {
											let i = this.shownPack.unlockedPackTrees.indexOf(pt);
											if (i == -1) {
												this.shownPack.unlockedPackTrees.push(pt);
											} else {
												this.shownPack.unlockedPackTrees.splice(i, 1);
											}
										}}
									/>
									{pt.name}
								</div>
							))}
						</div>
						<div>
							<input
								id="display-number"
								type="checkbox"
								checked={this.shownPack.displayNumberWorkshopInTooltip}
								onChange={() => this.shownPack.displayNumberWorkshopInTooltip = !this.shownPack.displayNumberWorkshopInTooltip}
							/>
							<label htmlFor="display-number">Display in tooltip the max number of workers</label>
						</div>
						<div>
							<input
								id="allow-workshop"
								type="checkbox"
								checked={this.shownPack.allowWorkshops}
								onChange={() => this.shownPack.allowWorkshops = !this.shownPack.allowWorkshops}
							/>
							<label htmlFor="allow-workshop">Allow workshops to be bought</label>
						</div>
						<div>
							<input
								id="allow-save"
								type="checkbox"
								checked={this.shownPack.unlockSaves}
								onChange={() => this.shownPack.unlockSaves = !this.shownPack.unlockSaves}
							/>
							<label htmlFor="allow-save">Allow saves</label>
						</div>
						Price:
						<input
							value={this.shownPack.price}
							onChange={(e) => this.shownPack.price = parseInt(e.currentTarget.value)}
						/>
						<br />
						<div className="grid-row">
							{this.shownPack.recipes.map((recipe) => (
								<div onClick={() => this.removeRecipePack(recipe)}>
									<ItemSlotComponent
										gameData={this.props.gameData}
										item={recipe.result}
										quantity={1}
									/>
								</div>
							))}
						</div>
						<button onClick={() => this.addRecipePack()}>Add selected item</button>
						<div>
							<h3>Pre-requisites:</h3>
							{this.shownPack.prerequisites.map((pr) => (
								<div onClick={() => this.removePrerequisite(pr)}>
									{pr.name}
								</div>
							))}

							<select onChange={(e) => this.onSelectedPrerequisiteChange(e)}>
								{this.shownPackTree.packs.map((pack) => (
									pack != this.shownPack && (
										<option value={pack.name}>
											{pack.name}
										</option>
									)
								))}
							</select>
							<button onClick={() => this.addPackPrerequisite()}>Add prerequisite</button>
						</div>
						Icon:<br />
						<IconPickerComponent
							onIconClick={(x, y) => { this.shownPack.iconX = x; this.shownPack.iconY = y }}
						/>
					</div>
				)}
				{
					/**
					 * Modifying an achievement section
					 */
				}
				{this.shownAchievement != null && (
					<div className="mf-column">
						<div>
							<div className="slot">
								<ItemImageComponent x={this.shownAchievement.iconX} y={this.shownAchievement.iconY}/>
							</div>
						</div>
						<div>
							Id:
							<input
								value={this.shownAchievement.id}
								onChange={e => this.shownAchievement.id = e.target.value}
							/>
						</div>
						<div>
							Name:
							<input
								value={this.shownAchievement.name}
								onChange={e => this.shownAchievement.name = e.target.value}
							/>
						</div>
						<div>
							Condition:
							<select value={this.shownAchievement.condition.constructor.name} onChange={(e) => this.changeCondition(e.target.value)}>
								{conditionsForAchievements.map(([c, factory]) => (
									<option value={c}>
										{c}
									</option>
								))}
							</select>
							<div className="mf-column">
								{this.shownAchievement.condition instanceof PackBoughtCondition ? (
									<div>
										Pack to buy: <select
											value={this.shownAchievement.condition.pack.id}
											onChange={e => {
												// @ts-ignore
												this.shownAchievement.condition.pack = this.gameData.packs.find(p => p.id == e.target.value)
											}}
										>
											{this.gameData.packs.map(p => (
												<option value={p.id}>
													{p.name}
												</option>
											))}
										</select>
									</div>
								) : this.shownAchievement.condition instanceof MoneyReachedCondition ? (
									<div>
										Money to reach:
										<input
											value={this.shownAchievement.condition.amount}
											onChange={(e) => {
												// @ts-ignore
												this.shownAchievement.condition.amount = parseInt(e.target.value)
											}}
										/>
									</div>
								) : (
									"The developer hasn't code the modification interface for this condition"
								)}
							</div>
						</div>
						<div>
							<IconPickerComponent
								onIconClick={(x, y) => { this.shownAchievement.iconX = x; this.shownAchievement.iconY = y }}
							/>
						</div>
					</div>
				)}
				<div className="mf-column">
					<div>
						Worker prices
					</div>
					<div>
						<button onClick={() => this.gameData.workerPrices.push([])}>Add row</button>
						<div>
							{this.gameData.workerPrices.map(row => (
								<div>
									<button onClick={() => row.push(0)}>Add price</button>
									{row.map((n, i) => (
										<input onChange={e => row[i] = parseInt(e.target.value)} value={n} />
									))}
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		);
	}

	componentDidMount(): void {
		// Launch the periodic save
		this.saveIntervalId = setInterval(() => {
			this.refreshJSONSave()
		}, 5000);
	}

	public refreshJSONSave() {
		let serializer = createGameSerializer();
		let dataRaw = serializer.serialize(this.props.gameData);
		let dataRawJson = JSON.stringify(dataRaw, null, "\t");

		this.jsonSave = dataRawJson;
		localStorage.setItem("savedGameData", dataRawJson);
		this.lastTimeSaved = new Date();
	}

	public setImage(event: React.MouseEvent<HTMLImageElement>) {
		var imgElement = document.getElementById("sprite-image");
		var x = Math.floor((event.pageX - imgElement.offsetLeft) / 32);
		var y = Math.floor((event.pageY - imgElement.offsetTop) / 32);
		console.log(imgElement.offsetTop);

		this.shownItem.x = x;
		this.shownItem.y = y;
	}

	public getItemsInGrid(): Item[][] {
		var countColumn = Math.ceil(this.props.gameData.items.length / 5);
		return Util.separateArrayGrid<Item>(this.props.gameData.items, 5, countColumn);
	}

	public removeRecipePack(recipe: Recipe) {
		this.shownPack.recipes.splice(this.shownPack.recipes.indexOf(recipe), 1);
	}

	public addPack() {
		// Check id
		let id = this.newPackText;
		if (this.gameData.packs.find(p => p.id == id)) {
			alert("ID already used");
			return;
		}

		var pack = new Pack(id, "New pack", 0, 0, 10, 10, [], 100);
		this.shownPackTree.packs.push(pack);
	}

	public addPackTree() {
		// Check id
		let id = this.newPackTreeText;
		if (this.gameData.packTrees.find(pt => pt.id == id)) {
			alert("ID already used");
			return;
		}
		let packTree = new PackTree(id, "", 0, 0);
		this.props.gameData.packTrees.push(packTree);
	}

	public addRecipePack() {
		var recipe = this.props.gameData.getRecipeForItem(this.selectedItem);
		if (recipe == null) {
			return;
		}

		this.shownPack.recipes.push(recipe);
	}

	public removePrerequisite(p: Pack) {
		let id = this.shownPack.prerequisites.indexOf(p);
		this.shownPack.prerequisites.splice(id, 1);
	}

	public onSelectedPrerequisiteChange(e: React.FormEvent<HTMLSelectElement>) {
		this.selectedPackPrerequisite = this.props.gameData.packs.find((p) => p.name == e.currentTarget.value);
	}

	public addRecipe(item: Item) {
		var recipe = new Recipe([], item, 1, 3);

		this.props.gameData.recipes.push(recipe);
	}

	public addItemRecipe(recipeItem: Item, item: Item) {
		var recipe = this.props.gameData.getRecipeForItem(recipeItem);
		if (recipe == null) {
			return;
		}

		recipe.items.push(new ItemQuantity(item, 1));
	}

	public computeRentability(item: Item): number {
		let recipe = this.props.gameData.getRecipeForItem(item);
		if (recipe == null) {
			return 0;
		}

		return recipe.quantity * item.price / recipe.time;
	}

	public computeGainedRentability(item: Item): number {
		let recipe = this.props.gameData.getRecipeForItem(item);
		if (recipe == null) {
			return 0;
		}

		let rentability = this.computeRentability(item);
		let baseRentability = recipe.items.map((q) => this.computeRentability(q.item) * q.quantity)
			.reduce((p, v) => p+v, 0);

		return rentability - baseRentability;
	}

	public addPackPrerequisite() {
		if (this.selectedPackPrerequisite == null) {
			return;
		}

		this.shownPack.prerequisites.push(this.selectedPackPrerequisite);
	}

	public removeItemRecipe(recipeItem: Item, itemQ: ItemQuantity) {
		var recipe = this.props.gameData.getRecipeForItem(recipeItem);

		recipe.items.splice(recipe.items.indexOf(itemQ), 1);
	}

	public removeRecipe(recipeItem: Item) {
		var recipe = this.props.gameData.getRecipeForItem(recipeItem);

		this.props.gameData.removeRecipe(recipe);
	}

	public addItem() {
		// Check id
		let id = this.newItemText;
		if (this.gameData.items.find(i => i.id == id)) {
			alert("ID already used");
			return;
		}
		var item = new Item(id, "", 0, 0, 0);
		this.props.gameData.items.push(item);

		this.shownItem = item;
	}

	packsContainer: HTMLDivElement;
	readonly PACK_GRID_SNAP = 22.5;

	public onPackBackdropClick(e: React.MouseEvent<HTMLDivElement>) {
		if (this.shownPack == null) {
			return;
		}

		let x = e.pageX - this.packsContainer.offsetLeft;
		let y = e.pageY - this.packsContainer.offsetTop;

		// The middle of the container should be the center of the x-axis
		x = x - this.packsContainer.clientWidth / 2;

		this.shownPack.x = Math.round(x / this.PACK_GRID_SNAP) * this.PACK_GRID_SNAP;
		this.shownPack.y = Math.round(y / this.PACK_GRID_SNAP) * this.PACK_GRID_SNAP;
	}

	private removeItem(item: Item) {
		this.gameData.removeItem(item);

		if (this.shownItem == item) {
			this.shownItem = null;
		}
	}

	private changeCondition(value: string) {
		let [conditionName, factory] = conditionsForAchievements.find(([c, f]) => c == value);

		this.shownAchievement.condition = factory(this.gameData, this.shownAchievement);
	}

	private addAchievement() {
		let achievement = new Achievement();

		// Give the first condition in the list as default
		let [condition, factory] = conditionsForAchievements[0];
		achievement.condition = factory(this.gameData, achievement);

		this.gameData.achievements.push(achievement);
	}

	private deletePack() {
		this.gameData.removePack(this.shownPack);
		this.shownPack = null;
	}
}

interface IconPickerProps {
	onIconClick: (x: number, y: number) => void;
}

class IconPickerComponent extends Component<IconPickerProps> {
	imgElement: HTMLImageElement;

	render(): ReactNode {
		return (
			<img onClick={(e) => this.onClick(e)} ref={i => this.imgElement = i} src="images/items.png" />
		);
	}

	public onClick(event: React.MouseEvent<HTMLImageElement>) {
		var x = Math.floor((event.pageX - this.imgElement.offsetLeft) / 32);
		var y = Math.floor((event.pageY - this.imgElement.offsetTop) / 32);

		this.props.onIconClick(x, y);
	}
}