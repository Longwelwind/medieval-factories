import GameData from "../game/GameData";
import Game from "../game/Game";
import Pack from "../game/Pack";
let gameanalytics = require("gameanalytics");
process.env
/**
 * The variables GA_API_KEY and GA_API_GAME are defined by Webpack's
 * Define plugin, see webpack.config.js for more info.
 */
export default class Analytics {
	readonly currencies = ["Gold"];
	readonly itemTypes = ["Sell"];
	readonly build = "0.0.1";

	enabled: boolean;
	game: Game;

	public constructor() {
	}

	public init() {
		this.enabled = env.GA_API_KEY != undefined && env.GA_API_GAME != undefined;

		if (this.enabled) {
			if (env.DEBUG) {
				gameanalytics.GameAnalytics.setEnabledInfoLog(true);
				gameanalytics.GameAnalytics.setEnabledVerboseLog(true);
			}

			gameanalytics.GameAnalytics.configureAvailableResourceCurrencies(this.currencies);
			gameanalytics.GameAnalytics.configureAvailableResourceItemTypes(this.itemTypes);
			gameanalytics.GameAnalytics.configureBuild(GameData.VERSION);

			gameanalytics.GameAnalytics.initialize(env.GA_API_GAME, env.GA_API_KEY);
		}
	}

	public start(game: Game) {
		this.game = game;

		if (this.enabled) {
			// Initializing events from the game
			this.game.onBuyPack.addListener((pack: Pack) => {
				gameanalytics.GameAnalytics.addDesignEvent("Pack:Buy:" + pack.id);
			});

			this.game.onAchievement.addListener(a => {
				gameanalytics.GameAnalytics.addDesignEvent("Achievement:Get:" + a.id);
			});
		}
	}

	public reportError(error: Error) {
		if (this.enabled) {
			let message = error.name + ": " + error.message + "\n" + error.stack;

			gameanalytics.GameAnalytics.addErrorEvent(
				gameanalytics.EGAErrorSeverity.Error,
				message
			);
		}
	}

	public isReady(): boolean {
		/**
		 * `isSdkReady` is supposed to be private, but
		 * you know, since this is already a dirty hack...
		 * "in for a penny, in for a stag"
		 */
		return this.enabled ? gameanalytics.GameAnalytics.isSdkReady(true) : true;
	}
} 