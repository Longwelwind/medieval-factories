import Recipe from "./Recipe";
import Task from "./Task";
import Game from "./Game";
import * as Util from "./Util";

import { observable } from "mobx";

export default class Worker {
	nonSerializedProperties = [
		"task", "state", "inventoryChangeListener"
	];

	game: Game;
	recipe: Recipe = null;
	@observable task: Task;
	@observable state: State = State.STOPPED;
	inventoryChangeListener;

	public constructor(game: Game) {
		this.game = game;
	}

	public start() {
		this.tryBeginWork();

		this.game.inventory.onChange.push(this.inventoryChangeListener = () => {
			this.onInventoryChange();
		});
	}

	private tryBeginWork(): boolean {
		if (this.recipe == null) {
			return false;
		}

		// We check if we have the ressourcess
		var hasRessources = this.recipe.items.every((i) => {
			return this.game.inventory.has(i.item, i.quantity);
		});

		if (!hasRessources) {
			return;
		}

		// Can we begin the production ?
		if (this.state != State.STOPPED) {
			return;
		}

		// We begin the production
		this.task = this.game.taskManager.addTask(() => {
			this.onTaskFinish();
		}, this.recipe.time);
		
		this.state = State.WORKING;

		// We remove all the ressources
		this.recipe.items.forEach((i) => {
			this.game.inventory.removeItem(i.item, i.quantity);
		});
	}

	/**
	 * Change the recipe of the worker.
	 * If the worker was currently working on the old recipe, the work
	 * will be interrupted (and the materials not refunded)
	 * @param newRecipe The new recipe for the worker
	 */
	changeRecipe(newRecipe: Recipe): void {
		if (this.task != null) {
			this.game.taskManager.removeTask(this.task);
			this.task = null;
			this.state = State.STOPPED;
		}

		this.recipe = newRecipe;
		this.tryBeginWork();
	}

	private onInventoryChange() {
		if (this.state != State.STOPPED) {
			return;
		}

		this.tryBeginWork();
	}

	private onTaskFinish() {
		this.state = State.STOPPED;
		this.task = null;

		// We add the result item
		this.game.inventory.addItem(this.recipe.result, this.recipe.quantity);

		// We try to do one cycle again
		this.tryBeginWork();
	}

	public delete() {
		this.game.inventory.onChange.splice(
			this.game.inventory.onChange.indexOf(this.inventoryChangeListener), 1
		);
	}
}

enum State {
	WORKING = 0,
	PAUSED = 1,
	STOPPED = 2
}
