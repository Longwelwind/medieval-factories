export type Callback<A, R> = (A) => R;

export class EventManager<A = null, R = void> {
	listeners: Callback<A, R>[] = [];

	addListener(listener: Callback<A, R>): Callback<A, R> {
		this.listeners.push(listener);

		return listener;
	}

	removeListener(listener: Callback<A, R>): void {
		let i = this.listeners.indexOf(listener);

		if (i == -1) {
			return;
		}

		this.listeners.splice(i, 2);
	}

	fire(args: A = null) {
		this.listeners.forEach(c => {
			c(args);
		});
	}
}
