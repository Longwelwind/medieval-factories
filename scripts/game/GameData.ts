import Item from "./Item";
import Recipe from "./Recipe";
import Pack from "./Pack";

import { observable } from "mobx";
import PackTree from "./PackTree";
import IDable from "../serialization/IDable";
import Achievement from "./achievement/Achievement";
import {PackBoughtCondition} from "./achievement/Condition";

export default class GameData implements IDable {
	readonly BASE_PRICE: number = 10;
	readonly PRICE_MULTIPLIER: number = 1.2;
	static readonly VERSION: string = "0.0.1";

	public nonSerializedProperties = [
		"VERSION"
	];

	@observable items: Item[] = [];
	@observable recipes: Recipe[] = [];
	@observable packTrees: PackTree[] = [];
	@observable achievements: Achievement[] = [];
	@observable startingPacks: Pack[] = [];
	@observable workerPrices: number[][] = [];

	// To be serializable, a class must have an ID, but since GameData will only ever be serialized once,
	// we can attribute a dummy ID
	id: string = "game-data";

	get packs(): Pack[] {
		return [].concat(...this.packTrees.map(pt => pt.packs));
	}

	public hasRecipe(item: Item): boolean {
		return this.getRecipeForItem(item) != null;
	}

	public getRecipeForItem(item: Item): Recipe {
		let recipes = this.getRecipesForItem(item);
		return recipes.length > 0 ? recipes[0] : null;
	}

	/**
	 * Returns the PackTree of `pack`, or null if the pack doesn't
	 * belong to any PackTree.
	 * @param pack
	 */
	getPackTree(pack: Pack): PackTree {
		return this.packTrees.find(pt => pt.packs.indexOf(pack) != -1);
	}

	public getRecipesForItem(item: Item): Recipe[] {
		return this.recipes.filter((r) => {
			return r.result == item;
		});
	}

	public removeItem(item: Item) {
		let recipe = this.getRecipeForItem(item);

		// We remove a potential recipe that makes this item
		if (recipe != null) {
			this.removeRecipe(recipe);
		}

		// We remove all the ingredients of this item for all recipes
		this.recipes.forEach((recipe) => {
			var concernedItemQ = recipe.items.filter((itemQ) => { return itemQ.item == item; })[0];
			if (concernedItemQ != undefined) {
				recipe.items.splice(recipe.items.indexOf(concernedItemQ), 1);
			}
		});

		this.items.splice(this.items.indexOf(item), 1);
	}

	public removeRecipe(recipe: Recipe) {
		// We check if there's a pack of this recipe
		this.packs.forEach((pack) => {
			if (pack.recipes.indexOf(recipe) != -1) {
				pack.recipes.splice(pack.recipes.indexOf(recipe), 1);
			}
		});

		// We remove the recipe
		this.recipes.splice(this.recipes.indexOf(recipe), 1);
	}

	/**
	 * Returns the price of the `worker`-th worker of the `workshop`-th workshop.
	 * @param workshop The index of the workshop to buy the worker
	 * @param worker The index of the worker to buy
	 */
	getWorkerPrice(workshop: number, worker: number): number {
		if (this.workerPrices.length <= workshop) {
			return 0;
		}

		if (this.workerPrices[workshop].length <= worker) {
			return 0;
		}

		return this.workerPrices[workshop][worker];
	}

	/**
	 * Removes a pack from the game data, also removes
	 * references to this pack in dependencies or achievements
	 * @param pack Pack to remove
	 */
	removePack(pack: Pack): void {
		let packTree = this.getPackTree(pack);

		if (packTree == null) {
			return;
		}

		packTree.packs.splice(packTree.packs.indexOf(pack), 1);

		// Remove in achievements
		this.achievements.forEach(a => {
			if (a.condition instanceof PackBoughtCondition) {
				if (this.packs.length == 0) {
					throw new Error("At least one pack should be available to replace the deleted one in achievement");
				}
				a.condition.pack = this.packs[0];
			}
		});

		// Remove in pre-requisites
		this.packs.forEach(p => {
			let i = p.prerequisites.indexOf(pack);
			if (i != -1) {
				p.prerequisites.splice(i, 1);
			}
		});

		// Remove in starting packs
		let i = this.startingPacks.indexOf(pack);
		if (i != -1) {
			this.startingPacks.splice(i, 1);
		}
	}

	/**
	 * Returns the width of the pack tree,
	 * which corresponds to the double of the maximum of the absolute value
	 * of the {@link Pack.x} value of each packs, with an added margin.
	 * TODO: GameData should not contain anything interface related, find somewhere else to put that
	 */
	getWidthPackTree(): number {
		const PACK_TREE_X_MARGIN = 50;
		return 2 * (this.packs.reduce((s, e) => Math.max(s, Math.abs(e.x)), 0) + PACK_TREE_X_MARGIN);
	}

	/**
	 * Returns the height of the pack tree, which corresponds to the maximum of the {@link Pack.y}
	 * value of each packs, with an added margin.
	 * TODO: GameData should not contain anything interface related, find somewhere else to put that
	 */
	getHeightPackTree(): number {
		const PACK_TREE_BOTTOM_MARGIN = 25;
		return this.packs.reduce((s, e) => Math.max(s, e.y), 0) + PACK_TREE_BOTTOM_MARGIN;
	}
}
