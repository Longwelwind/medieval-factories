import Item from "./Item";
import ItemQuantity from "./ItemQuantity";
import * as Util from "./Util";

import { observable } from "mobx";
import IDable from "../serialization/IDable";

export default class Recipe implements IDable {
	id: string;
	@observable items: ItemQuantity[];
	public result: Item;
	@observable quantity: number;
	@observable time: number;

	public constructor(items: ItemQuantity[], result: Item, quantity: number, time: number) {
		// Since there's only one recipe per item, we can safely share the ID
		this.id = result != null ? result.id : "";
		this.items = items;
		this.result = result;
		this.time = time;
		this.quantity = quantity;
	}

	public contains(item: Item): boolean {
		return this.items.some((itemQuantity) => { return itemQuantity.item == item; });
	}
}
