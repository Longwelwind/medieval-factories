import {observable} from "mobx";
import Pack from "./Pack";
import IDable from "../serialization/IDable";

export default class PackTree implements IDable {
    @observable id: string;
    @observable name: string;

    // Image coordinate
    @observable x: number;
    @observable y: number;

	/**
	 * Whether the PackTree is unlocked by default
	 */
	@observable preUnlocked: boolean = false;

    @observable packs: Pack[];

    constructor(id: string, name: string, x: number, y: number) {
        this.id = id;
        this.name = name;
        this.x = x;
        this.y = y;
        this.packs = [];
    }
}