import ItemQuantity from "./ItemQuantity";
import Item from "./Item";
import * as EventEmitter from "eventemitter3";
import Game from "./Game";

import { observable } from "mobx";

export default class Inventory {
	public nonSerializedProperties = ["onChange"];

	@observable items: ItemQuantity[];
	public onChange: ((item: Item) => void)[] = [];

	public constructor(items: ItemQuantity[]) {
		this.items = items;
	}

	public addItem(item: Item, quantity: number) {
		if (quantity < 0) {
			throw new Error("addItem: Trying to add negative quantity");
		}

		var itemQuantity = this.getItemQuantity(item);
		if (itemQuantity == null) {
			itemQuantity = new ItemQuantity(item, 0);
			this.items.push(itemQuantity);
		}

		itemQuantity.quantity += quantity;

		if (itemQuantity.quantity < 0) {
			this.items.splice(this.items.indexOf(itemQuantity), 1);
		}

		this.onChange.forEach((f) => {
			f(item);
		});
	}

	public removeItem(item: Item, quantity: number) {
		if (quantity < 0) {
			throw new Error("addItem: Trying to remove negative quantity");
		}

		var itemQuantity = this.getItemQuantity(item);
		if (itemQuantity == null) {
			return;
		}

		itemQuantity.quantity -= quantity;

		if (itemQuantity.quantity < 0) {
			this.items.splice(this.items.indexOf(itemQuantity), 1);
		}

		this.onChange.forEach((f) => {
			f(item);
		});
	}

	public has(item: Item, quantity: number): boolean {
		return this.getQuantity(item) >= quantity;
	}

	public getQuantity(item: Item): number {
	   var itemQuantity = this.getItemQuantity(item);
	   if (itemQuantity != null) {
		   return itemQuantity.quantity;
	   } else {
		   return 0;
	   }
	}

	private getItemQuantity(item: Item): ItemQuantity {
		return this.items.filter((iq) => { return iq.item == item; })[0];
	}
}
