import Recipe from "./Recipe";
import * as Util from "./Util";

import { observable } from "mobx";
import IDable from "../serialization/IDable";

export default class Item implements IDable {
	@observable id: string;
	@observable name: string;
	@observable x: number;
	@observable y: number;
	@observable price: number;

	constructor(id: string, name: string, x: number, y: number, price: number) {
		this.id = id;
		this.name = name;
		this.x = x;
		this.y = y;
		this.price = price;
	}
}
	