import Item from "./Item";
import Game from "./Game";
import * as Util from "./Util";

import { observable } from "mobx";

export default class ItemQuantity {
	public item: Item;
	@observable quantity: number;

	public constructor(item: Item, quantity: number) {
		this.item = item;
		this.quantity = quantity;
	}
}
