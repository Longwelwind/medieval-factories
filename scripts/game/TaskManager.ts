import Task from "./Task";

export default class TaskManager {
	private tasks: Task[] = [];

	public addTask(fn: () => void, delay: number): Task {
		var task = new Task(fn, delay);
		this.tasks.push(task);
		return task;
	}

	public update(delta: number) {
		while (
			delta > 0
			|| (
				this.getNearestTask() != null
				&& this.getNearestTask().getTimeRemaining() == 0
			)
		) {
			var task = this.getNearestTask();
			if (task == null) {
				return;
			}

			var usedTime;
			if (delta < task.getTimeRemaining()) {
				// We have not enough time to finish a task
				// We will simply progress each tasks
				usedTime = delta;
			} else {
				// We can execute at least a task
				usedTime = task.getTimeRemaining();
			}

			// We progress the advancement of all tasks
			this.tasks.forEach((t) => {
				t.advancement += usedTime;
			});

			// We now execute the task if we had the time to do so
			if (usedTime >= task.getTimeRemaining()) {
				this.executeTask(task);
				task.advancement = 0;
			}

			delta -= usedTime;
		}
	}

	private executeTask(task: Task) {
		task.todo();
		// We remove the task
		this.tasks.splice(this.tasks.indexOf(task), 1);
	}

	public getNearestTask(): Task {
		var lowestTask = this.tasks[0];
		this.tasks.forEach((t) => {
			if (t.getTimeRemaining() < lowestTask.getTimeRemaining()) {
				lowestTask = t;
			}
		});
		return lowestTask;
	}

	public removeTask(task: Task) {
		let i = this.tasks.indexOf(task);
		if (i == -1) {
			return;
		}

		this.tasks.splice(i, 1);
	}
}
