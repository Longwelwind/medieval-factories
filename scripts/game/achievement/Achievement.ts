import {observable} from "mobx";
import Condition from "./Condition";
import Game from "../Game";

/**
 * An achievement in the game
 */
export default class Achievement {
	/**
	 * The serialization id of the achievement
	 */
	@observable id: string = "";

	/**
	 * The displayed name of the achievement
	 */
	@observable name: string = "";

	/**
	 * The x coordinate of the icon for the achievement
	 */
	@observable iconX: number = 0;

	/**
	 * The y coordinate of the icon for the achievement
	 */
	@observable iconY: number = 0;

	/**
	 * The condition to accomplish to get this achievement
	 */
	@observable condition: Condition;

	/**
	 * Called at the start of a game.
	 * Allows the condition to initialize and react when the player
	 * achieves the condition.
	 * @param game Game to start with
	 */
	start(game: Game) {
		this.condition.start(game);
	}

	/**
	 * Called everytime the game updates.
	 * Allows the condition to track things during an update
	 * @param game Game to update with
	 */
	update(game: Game) {
		this.condition.update(game);
	}
}