import Game from "../Game";
import {observable} from "mobx";
import Pack from "../Pack";
import Achievement from "./Achievement";

/**
 * Represents the condition of the achievement the player must
 * accomplish to complete the achievement.
 */
export default abstract class Condition {
	achievement: Achievement;

	constructor(achievement: Achievement) {
		this.achievement = achievement;
	}

	/**
	 * Called when the game starts, allows the condition to
	 * initialize the callbacks to check if the player
	 * accomplished the achievement
	 * @param game
	 */
	abstract start(game: Game): void;

	/**
	 * Called everytime the game updates, allows the condition
	 * to check if it is accomplished
	 * @param game
	 */
	abstract update(game: Game): void;
}

/**
 * A condition that is accomplished when the player buys a specific pack
 */
export class PackBoughtCondition extends Condition {
	@observable pack: Pack;

	constructor(achievement: Achievement, pack: Pack) {
		super(achievement);
		this.pack = pack;
	}

	start(game: Game): void {
		game.onBuyPack.addListener((pack) => {
			if (this.pack == pack) {
				game.accomplishAchievement(this.achievement);
			}
		});
	}

	update(game: Game): void {
		// Nothing
	}
}

/**
 * A condition that is accomplished when the players owns a certain amount of money
 */
export class MoneyReachedCondition extends Condition {
	@observable amount: number;

	start(game: Game): void {
		game.onMoneyChange.addListener(() => {
			if (game.money >= this.amount) {
				game.accomplishAchievement(this.achievement);
			}
		})
	}

	update(game: Game): void {
		// Nothing
	}
}