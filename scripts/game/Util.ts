export function separateArrayGrid<E>(list: E[], columnCount: number, rowCount: number, fillRows?: boolean, fillColumns?: boolean, page?: number): E[][] {
	if (page == undefined) {
		page = 0;
	}
	if (fillRows == undefined) {
		fillRows = false;
	}
	if (fillColumns == undefined) {
		fillColumns == false;
	}
	var pageSize = columnCount * rowCount;
	var displayedList = list.slice(page*pageSize, (page+1)*pageSize);
	var count = displayedList.length;
	
	var grid = [];
	for (var i = 0;i < rowCount;i++) {
		var row = [];
		for (var j = 0;j < columnCount;j++) {
			var index = i*columnCount + j;
			if (index < count) {
				row.push(displayedList[index]);
			} else if (fillRows) {
				row.push(null);
			}
		}
		if (fillColumns || (!fillColumns && row.some((e) => { return e != null; }))) {
			grid.push(row);
		}
	}
	
	return grid;
}

export function httpGetAsync(theUrl: string, callback: (status: number, response: string) => void)
{
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { 
		if (xmlHttp.readyState == 4) {
			if (xmlHttp.status == 200) {
				return callback(200, xmlHttp.responseText);
			} else {
				return callback(xmlHttp.status, null);
			}
		}
	};
	xmlHttp.open("GET", theUrl, true); // true for asynchronous 
	xmlHttp.send(null);
}

let units = ["k", "m", "b"];

export function formatQuantity(quantity: number) {
	let i: number = 0;
	while (quantity / Math.pow(1000, i + 1) > 1) {
		i++;
	}
	let unit = "";
	if (i > 0) {
		quantity = Math.floor(quantity / Math.pow(1000, i));
		unit = units[i - 1];
	}

	return quantity + unit;
}

export function formatTime(time: number) {
	let f = "";
	
	let hours = Math.floor(time / 3600);
	time -= hours * 3600;

	if (hours > 0) {
		f += hours + "h ";
	}

	let minutes = Math.floor(time / 60);
	time -= minutes * 60;

	if (hours > 0 || minutes > 0) {
		f += minutes + "m ";
	}

	let seconds = Math.floor(time);
	f += seconds + "s";

	return f;
}