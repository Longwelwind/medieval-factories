
import { observable } from "mobx";

export default class Task {
	
	@observable
	public advancement: number = 0;
	
	public constructor(public todo: () => void, public delay: number) {
		
	}
	
	public getTimeRemaining(): number {
		return this.delay - this.advancement;
	}
}