import Item from "./Item";
import Recipe from "./Recipe";
import Worker from "./Worker";
import Pack from "./Pack";
import Inventory from "./Inventory";
import TaskManager from "./TaskManager";
import * as Util from "./Util";
import GameData from "./GameData";

import { observable } from "mobx";
import Task from "./Task";
import {EventManager} from "./EventManager";
import Achievement from "./achievement/Achievement";
import {Serializer} from "../serialization/Serialization";
import PackTree from "./PackTree";

export default class Game {
	public static i: Game;

	public nonSerializedProperties = [
		"WORKERS_PER_WORKSHOP", "WORKSHOP_BASE_PRICE", "WORKSHOP_EXPONENTIAL",
		"WORKER_BASE_PRICE", "WORKER_EXPONENTIAL", "TIME_BETWEEN_SAVE",
		"taskManager", "onDiscover", "onBuyWorker", "onSell", "onBuyPack",
		"currentlyCraftingTask", "onMoneyChange", "onAchievement", "serializer"
	];

	WORKSHOP_BASE_PRICE: number = 1000;
	WORKSHOP_EXPONENTIAL: number = 1.5;
	TIME_BETWEEN_SAVE: number = 5;

	WORKER_BASE_PRICE: number = 10;
	WORKER_EXPONENTIAL: number = 1.3;

	/**
	 * The x-coordinate of the question mark image.
	 */
	static QUESTION_MARK_X: number = 15;

	/**
	 * The y-coordinate of the question mark image.
	 */
	static QUESTION_MARK_Y: number = 83;

	// To be serializable, a class must have an ID field, but since Game will
	// only ever be instantiated once, we can put a dummy ID
	id: string = "game";
	serializer: Serializer;
	gameData: GameData;
	@observable workshops: Worker[][] = [[]]; // First array is for workshops
	@observable money: number = 0;
	@observable boughtPacks: Pack[] = [];
	inventory: Inventory;
	@observable autosellItems: Item[] = [];
	@observable elapsedTime: number = 0;
	@observable autoSaveEnabled: boolean = false;
	@observable lastSaveTime: number = 0;
	@observable timeSinceLastSave: number;

	/**
	 * The version of GameData at which this game was created.
	 * If retro-compatibility is ensured, then this game can be used
	 * with any GameData of version higher than this.
	 */
	version: string;

	@observable debugMode: boolean;
	@observable currentlyCraftingRecipe: Recipe;
	@observable currentlyCraftingTask: Task;
	@observable completedAchievements: Achievement[] = [];
	taskManager: TaskManager = new TaskManager();

	onBuyWorker = new EventManager<Worker>();
	onSell = new EventManager();
	onBuyPack = new EventManager<Pack>();
	onMoneyChange = new EventManager();
	onAchievement = new EventManager<Achievement>();

	/**
	 * Returns the maximum allowed number of workers for a workshop.
	 * Will iterate over all boughtPacks, and get the maximum value for
	 * {@link Pack.maxNumberWorkersPerWorkshop}
	 */
	get maxWorkersPerWorkshop(): number {
		return this.boughtPacks.reduce((s, pack) => Math.max(pack.maxNumberWorkersPerWorkshop, s), 1);
	}

	/**
	 * Returns whether the player can buy additional workshops
	 * Will iterate over all boughtPacks and get whether one of them
	 * has the value true for {@link Pack.allowWorkshops}
	 */
	get workshopsAllowed(): boolean {
		return this.boughtPacks.some(p => p.allowWorkshops);
	}

	/**
	 * Returns whether the player can saves and auto-saves.
	 * Will iterate over all {@link Game.boughtPacks} and check if at
	 * least one of them has the value for {@link Pack.unlockSaves}
	 */
	get saveUnlocked(): boolean {
		return this.boughtPacks.some(p => p.unlockSaves);
	}

	public constructor(gameData: GameData, serializer: Serializer) {
		Game.i = this;

		this.gameData = gameData;
		this.serializer = serializer;
		this.inventory = new Inventory([]);
		/**
		 * Just a default value, can be changed through Javascript's console
		 */
		this.debugMode = window.location.hostname.includes("localhost");
	}

	public buyWorkshop(): boolean {
		if (!this.trySpendMoney(this.getWorkshopPrice())) {
			return false;
		}
		this.workshops.push([]);
		return true;
	}

	public onInventoryChange(item: Item): void {
		// Autosell
		if (this.isAutosell(item) && this.inventory.getQuantity(item) != 0) {
			this.sellItem(item, this.inventory.getQuantity(item));
		}
	}

	public setAutosellItem(item: Item, autosell: boolean) {
		var index = this.autosellItems.indexOf(item);
		var contains = index != -1;
		if (autosell && !contains) {
			this.autosellItems.push(item);
			// We can already sell the items
			this.sellItem(item, this.inventory.getQuantity(item));
		} else if (!autosell && contains) {
			this.autosellItems.splice(index, 1);
		}
	}

	/**
	 * Marks an achievement as accomplished (whatever its condition),
	 * if an achievement was already marked as accomplished, does nothing.
	 * @param achievement Achievement to accomplish
	 */
	accomplishAchievement(achievement: Achievement): void {
		if (this.completedAchievements.indexOf(achievement) != -1) {
			return;
		}

		this.completedAchievements.push(achievement);

		this.onAchievement.fire(achievement);
	}

	/**
	 * Return whether an achievement has been accomplished
	 * @param achievement
	 */
	isAccomplished(achievement: Achievement): boolean {
		return this.completedAchievements.indexOf(achievement) != -1;
	}

	public deleteWorker(worker: Worker) {
		var workshop = this.workshops.filter((ws) => {
			return ws.indexOf(worker) != -1;
		})[0];

		worker.delete();

		workshop.splice(workshop.indexOf(worker), 1);
	}

	public isAutosell(item: Item) {
		return this.autosellItems.indexOf(item) != -1;
	}

	public canBuyWorker(workshop: number): boolean {
		return this.workshops[workshop].length < this.maxWorkersPerWorkshop;
	}

	public addBoughtPack(pack: Pack) {
		this.boughtPacks.push(pack);
	}

	public buyWorker(workshop: number): boolean {
		if (!this.trySpendMoney(this.gameData.getWorkerPrice(workshop, this.workshops[workshop].length))) {
			return false;
		}

		var worker = new Worker(this);
		this.workshops[workshop].push(worker);
		worker.start();

		this.onBuyWorker.fire(worker);

		return true;
	}

	public trySpendMoney(amount: number): boolean {
		if (this.money < amount) {
			return false;
		}

		this.removeMoney(amount);
		return true;
	}

	/**
	 * Give `amount` of the money to the player.
	 * @param amount Amount of money to give
	 */
	addMoney(amount: number): void {
		this.money += amount;
		this.onMoneyChange.fire();
	}

	/**
	 * Removes `amount` money of the player.
	 * Will not remove more money than the player has.
	 * @param amount Amount of money to remove
	 */
	removeMoney(amount: number): void {
		this.money = Math.max(0, this.money - amount);
		this.onMoneyChange.fire();
	}

	public isRecipeKnown(recipe: Recipe): boolean {
		return this.boughtPacks.some((p) => {
			return p.recipes.some((r) => {
				return r == recipe;
			});
		});
	}

	public buyPack(pack: Pack) {
		if (pack == null || !this.trySpendMoney(pack.price)) {
			return false;
		}

		this.addBoughtPack(pack);

		this.onBuyPack.fire(pack);

		return true;
	}

	public getWorkshopPrice() {
		return Math.round(
			Math.pow(this.workshops.length, this.WORKSHOP_EXPONENTIAL) * this.WORKSHOP_BASE_PRICE
		);
	}

	public sellItem(item: Item, quantity: number) {
		if (!this.inventory.has(item, quantity)) {
			return;
		}

		this.inventory.removeItem(item, quantity);
		this.addMoney(item.price * quantity);

		this.onSell.fire();
	}

	public firstStart() {
		// Add the starting packs
		// Assume they have a price of 0
		this.gameData.startingPacks.forEach(sp => {
			if (sp.price > 0) {
				console.warn("Starting pack \"" + sp.id + "\" doesn't have a price of 0");
			}
			this.buyPack(sp);
		});

		this.version = GameData.VERSION;

		this.save();
	}

	public start() {
		this.workshops.forEach((ws) => {
			ws.forEach((w) => {
				w.start();
			});
		});

		this.inventory.onChange.push((item) => {
			this.onInventoryChange(item);
		});

		this.gameData.achievements.forEach(a => {
			a.start(this);
		});
	}

	public update(delta: number) {
		this.elapsedTime += delta;

		this.taskManager.update(delta);

		// Trigger auto-save after timer
		if (this.autoSaveEnabled) {
			this.timeSinceLastSave += delta;
			if (this.timeSinceLastSave >= this.TIME_BETWEEN_SAVE) {
				this.save();
			}
		}
	}

	public updateToNext(): number {
		let nearestTask = this.taskManager.getNearestTask();
		if (nearestTask == null) {
			return -1;
		}

		let delta = nearestTask.getTimeRemaining();
		this.update(delta);

		return delta;
	}

	public getKnownItems(): Item[] {
		return this.gameData.items.filter((i) => {
			return this.isItemKnown(i);
		});
	}

	public isItemKnown(item: Item): boolean {
		var recipe = this.gameData.getRecipesForItem(item)[0];
		// Special case: we may know an item because we receive it by cheating
		return this.isRecipeKnown(recipe) || this.inventory.getQuantity(item) > 0;
	}

	public isPackBuyable(pack: Pack): boolean {
		return !this.isPackBought(pack) && pack.prerequisites.every((p) => this.isPackBought(p));
	}

	public isPackBought(pack: Pack): boolean {
		return this.boughtPacks.indexOf(pack) != -1;
	}

	craft(recipe: Recipe): void {
		if (!this.hasIngredientsToCraft(recipe)) {
			return;
		}

		if (this.currentlyCraftingTask != null) {
			return;
		}

		recipe.items.forEach(iq => this.inventory.removeItem(iq.item, iq.quantity));

		this.currentlyCraftingRecipe = recipe;
		this.currentlyCraftingTask = this.taskManager.addTask(() => {
			this.inventory.addItem(recipe.result, recipe.quantity);
			this.currentlyCraftingRecipe = null;
			this.currentlyCraftingTask = null;
		}, recipe.time);
	}

	hasIngredientsToCraft(recipe: Recipe): boolean {
		return recipe.items.every(iq => this.inventory.has(iq.item, iq.quantity));
	}

	/**
	 * Returns whether a PackTree is available to the player either because
	 * it is always available ({@link PackTree.preUnlocked}) or because
	 * the player has bought a pack that unlocks this PackTree.
	 * @param packTree The PackTree to check
	 */
	isPackTreeUnlocked(packTree: PackTree): boolean {
		return packTree.preUnlocked
			|| this.boughtPacks.some(p => p.unlockedPackTrees.indexOf(packTree) != -1);
	}

	/**
	 * Saves the game to {@link localStorage}.
	 * Resets timer for auto-saves
	 */
	save(): void {
		this.timeSinceLastSave = 0;
		this.lastSaveTime = Date.now();

		let data = this.serializer.serialize(this);
		localStorage.setItem("game", JSON.stringify(data));
	}
}
