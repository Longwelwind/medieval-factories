import Item from "./Item";
import Recipe from "./Recipe";
import GameData from "./GameData";
import * as Util from "./Util";

import { observable } from "mobx";
import IDable from "../serialization/IDable";
import PackTree from "./PackTree";

export default class Pack implements IDable {
	@observable id: string;
	@observable recipes: Recipe[];
	@observable iconX: number;
	@observable iconY: number;
	@observable x: number;
	@observable y: number;
	@observable name: string;
	@observable description: string = "";

	/**
	 * The pack trees that will be unlocked if this pack is bought
	 */
	@observable unlockedPackTrees: PackTree[] = [];

	/**
	 * Whether this pack unlocks saving and auto-saving
	 */
	@observable unlockSaves: boolean = false;

	@observable price: number;
	@observable prerequisites: Pack[] = [];
	@observable displayNumberWorkshopInTooltip: boolean = true;
	@observable maxNumberWorkersPerWorkshop: number = 0;
	@observable allowWorkshops: boolean = false;

	public constructor(id: string, name: string, iconX: number, iconY: number, x: number, y: number, recipes: Recipe[], price: number) {
		this.recipes = recipes;
		this.name = name;
		this.iconX = iconX;
		this.iconY = iconY;
		this.x = x;
		this.y = y;
		this.id = id;
		this.price = price;
	}
}
