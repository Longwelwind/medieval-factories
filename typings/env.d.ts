/**
 * Those variables are defined via the DefinePlugin in webpack.config.js
 */

interface EnvVariables {
	GA_API_GAME?: string,
	GA_API_KEY?: string,
	DEBUG: boolean
}

declare let env: EnvVariables;