# Medieval Factories

[![Gitlab CI](https://gitlab.com/Longwelwind/medieval-factories/badges/master/build.svg)](https://gitlab.com/Longwelwind/medieval-factories/pipelines)
[![GitHub license](https://img.shields.io/github/license/Longwelwind/medieval-factories.svg)](https://github.com/Longwelwind/medieval-factories/blob/master/LICENSE)
[![GitHub release](https://img.shields.io/github/release/Longwelwind/medieval-factories.svg)](https://GitHub.com/Longwelwind/medieval-factories/releases/)
![Project state](https://img.shields.io/badge/state-alpha-yellow.svg)

## How to launch locally

Clone the repo, and execute:

```bash
npm install
npm run start
```

Gitlab CI is configured to build the game and upload to Gitlab Pages when commits are pushed to master.